﻿using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.MapGeneration
{
    public class CaveCellTypesGenerator : MonoBehaviour
    {
        [SerializeField] private GridCellType _defaultType;
        [SerializeField] private List<CellTypeGenerationData> _cellTypeGenerationDatas;

        private Dictionary<Vector3Int, GridCellType> _cellTypes = new ();
        private List<Vector3Int> _nonEmptyCells = new ();
        private bool[,,] _caveData; 

        public List<(Vector3Int position, GridCellType type)> GetCellTypes(bool[,,] caveData)
        {
            _cellTypes.Clear();
            _nonEmptyCells.Clear();
            _caveData = caveData;

            _caveData.IterateAllPositions((x, y, z) =>
            {
                if (!_caveData[x, y, z])
                {
                    return;
                }

                var position = new Vector3Int(x, y, z);
                _cellTypes[position] = _defaultType;
                _nonEmptyCells.Add(position);
            });

            foreach(var cellTypeData in _cellTypeGenerationDatas)
            {
                for(int i = 0; i < cellTypeData.patchesAmount; i++)
                {
                    CreateCellTypePatch(cellTypeData.type, cellTypeData.patchRadius);
                }
            }

            return GetCellTypes();
        }

        private List<(Vector3Int position, GridCellType type)> GetCellTypes() => _cellTypes.Select(p => (p.Key, p.Value)).ToList();

        private void CreateCellTypePatch(GridCellType type, int patchRadius)
        {
            var randomPosition = _nonEmptyCells[UnityEngine.Random.Range(0, _nonEmptyCells.Count - 1)];

            for(int x = -patchRadius; x < patchRadius; x++)
            {
                for (int y = -patchRadius; y < patchRadius; y++)
                {
                    for (int z = -patchRadius; z < patchRadius; z++)
                    {
                        var p = new Vector3Int(randomPosition.x + x, randomPosition.y + y, randomPosition.z + z);
                        if (!_caveData.IsInArray(p) || !_caveData[p.x, p.y, p.z])
                        {
                            continue;
                        }

                        _cellTypes[p] = type;
                    }
                }
            }
        }

        [Serializable]
        private class CellTypeGenerationData
        {
            public GridCellType type;
            public int patchesAmount;
            public int patchRadius;
        }
    }
}