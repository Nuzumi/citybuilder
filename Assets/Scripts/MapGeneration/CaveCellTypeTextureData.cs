﻿using Assets.Scripts.Services.StructureGridServices;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.MapGeneration
{
    [CreateAssetMenu(menuName = "Data/" + nameof(CaveCellTypeTextureData))]
    public class CaveCellTypeTextureData : ScriptableObject
    {
        private static readonly List<Vector2> BasePositions = new ()
        {
            Vector2.zero,
            Vector2.up,
            Vector2.one,
            Vector2.right
        };

        [SerializeField] private Vector2Int _tilesAmount;
        [SerializeField] private List<CellTextureData> _textureDatas;

        public List<Vector2> GetUVsForType(GridCellType gridCellType)
        {
            return GetTileUvs(_textureDatas.Find(td => td.cellType == gridCellType).tilePosition);
        }

        private List<Vector2> GetTileUvs(Vector2Int tilePosition)
        {
            var tileSize = new Vector2(1f / _tilesAmount.x, 1f / _tilesAmount.y);
            return BasePositions
                .Select(p => new Vector2(p.x / _tilesAmount.x, p.y / _tilesAmount.y))
                .Select(p => p + tileSize * tilePosition)
                .ToList();
        }

        [Serializable]
        private class CellTextureData
        {
            public GridCellType cellType;
            public Vector2Int tilePosition;
        }
    }
}