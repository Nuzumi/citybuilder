using System.Collections.Generic;
using System;
using UnityEngine;
using Random = System.Random;

namespace Assets.Scripts.MapGeneration
{

    public class CaveGeneratorPerlin : MonoBehaviour
    {
        private static readonly List<Func<(Vector3Int, Vector3Int), Vector3Int>> Rotations = new List<Func<(Vector3Int position, Vector3Int caveSize), Vector3Int>>()
        {
            p => new Vector3Int(p.position.y, p.position.x, p.position.z),

            p => new Vector3Int(p.caveSize.x - p.position.y, p.position.x, p.position.z),

            p => new Vector3Int(p.position.x, p.position.z, p.position.y),

            p => new Vector3Int(p.position.x, p.position.z, p.caveSize.z - p.position.y),
        };

        [SerializeField] private Vector3Int _caveEnd;
        [SerializeField] private int _sead;
        [SerializeField] private float _cubeThreshold;
        [SerializeField] private float _perlinStep;
        [SerializeField] private int _circleRadius;
        [SerializeField] private int _initialPointCount;
        [SerializeField] private float _initialPointValue;
        [SerializeField] private int _initialSpawnRadius;

        [SerializeField] private PerlinValueCubes _florCubes;
        [SerializeField] private PerlinValueCubes _ceelingCubes;
        [SerializeField] private PerlinValueCubes _wallsCubes;

        private HashSet<Vector3Int> _positions = new();
        private float[,,] _positionInitialValues;
        private float[,,] _positionValues;
        private Random _random;
        private Vector2 _perlinOffset;
        private Vector3Int _floorCenter;
        private List<Vector3Int> _circlePositions = new();
        
        public bool[,,] GenerateCave()
        {
            SetupCaveValues();
            CalculateCave();

            return GetSimplifiedCubesArray();
        }

        private void SetupCaveValues()
        {
            Clear();
            SetNewPerlinOffset();
            SetCirclePositions();
            _floorCenter = new Vector3Int(_caveEnd.x / 2, _caveEnd.y / 2, _caveEnd.z / 2);
        }

        private void CalculateCave()
        {
            SetInitialCaveSpheres();
            BlureCave();
            InvertCave();
            AddPerlinNoiseToFloorAndCeiling();
            AddPerlinNoiseToWalls();
        }

        private void SetInitialCaveSpheres()
        {
            for (int i = 0; i < _initialPointCount; i++)
            {
                var position = GetRandomGridPosition(
                    new Vector3Int(_floorCenter.x - _initialSpawnRadius, 0, _floorCenter.z - _initialSpawnRadius),
                    new Vector3Int(_floorCenter.x + _initialSpawnRadius, _caveEnd.y, _floorCenter.z + _initialSpawnRadius));

                foreach (var circlePosition in _circlePositions)
                {
                    var p = position + circlePosition;
                    if (IsOnGrid(p))
                    {
                        SetInitialValue(p, _initialPointValue);
                    }
                }
            }
        }

        private void BlureCave()
        {
            IterateAllPositions(position =>
            {
                var value = 0f;
                var positionsOnGrid = 0;
                foreach (var circlePosition in _circlePositions)
                {
                    var p = position + circlePosition;
                    if (IsOnGrid(p))
                    {
                        positionsOnGrid++;
                        value += GetInitialValue(p);
                    }
                }

                value /= positionsOnGrid;

                SetValue(position, value);
            });
        }

        private void InvertCave()
        {
            IterateAllPositions(position =>
            {
                var positionValue = _positionValues[position.x, position.y, position.z];
                SetValue(position, positionValue < _cubeThreshold ? 1 : 0);
            });
        }

        private void AddPerlinNoiseToFloorAndCeiling()
        {
            IterateFloor(position =>
            {
                var perlinPosition = new Vector2((position.x + _perlinOffset.x) * _perlinStep, (position.z + _perlinOffset.y) * _perlinStep);
                var perlinValue = GetPerlinValue(perlinPosition);

                var floorCubes = _florCubes.GetCubesForValue(perlinValue);
                for (int i = 0; i < floorCubes; i++)
                {
                    var cubePosition = new Vector3Int(position.x, position.y + i, position.z);
                    SetValue(cubePosition, 1);
                }

                var ceelingCubes = _ceelingCubes.GetCubesForValue(perlinValue);
                for (int i = _caveEnd.y; i > _caveEnd.y - ceelingCubes; i--)
                {
                    var cubePosition = new Vector3Int(position.x, i, position.z);
                    SetValue(cubePosition, 1);
                }
            });
        }

        private void AddPerlinNoiseToWalls()
        {
            foreach (var rotation in Rotations)
            {
                SetNewPerlinOffset();
                IterateFloor(position =>
                {
                    var perlinPosition = new Vector2((position.x + _perlinOffset.x) * _perlinStep, (position.z + _perlinOffset.y) * _perlinStep);
                    var perlinValue = GetPerlinValue(perlinPosition);
                    var wallCubes = _wallsCubes.GetCubesForValue(perlinValue);

                    for (int i = 0; i < wallCubes; i++)
                    {
                        var cubePosition = new Vector3Int(position.x, position.y + i + 1, position.z);
                        SetValue(rotation((cubePosition, _caveEnd)), 1);
                    }
                });
            }
        }

        private void SetNewPerlinOffset()
        {
            _perlinOffset = new Vector2(_random.Next(500), _random.Next(500));
        }

        private float GetValue(Vector3Int position) => _positionValues[position.x, position.y, position.z];
        private float GetInitialValue(Vector3Int position) => _positionInitialValues[position.x, position.y, position.z];

        private void SetValue(Vector3Int position, float value) => _positionValues[position.x, position.y, position.z] = value;
        private void SetInitialValue(Vector3Int position, float value) => _positionInitialValues[position.x, position.y, position.z] = value;

        private bool IsOnGrid(Vector3Int position)
        {
            if (position.x < 0 || position.y < 0 || position.z < 0)
            {
                return false;
            }

            if (position.x >= _caveEnd.x || position.y >= _caveEnd.y || position.z >= _caveEnd.z)
            {
                return false;
            }

            return true;
        }

        private void Clear()
        {
            _positions.Clear();
            _positionValues = new float[_caveEnd.x + 1, _caveEnd.y + 1, _caveEnd.z + 1];
            _positionInitialValues = new float[_caveEnd.x + 1, _caveEnd.y + 1, _caveEnd.z + 1];
            _random = _sead == -1 ? new Random() : new Random(_sead);
        }

        private void IterateAllPositions(Action<Vector3Int> action)
        {
            for (int x = 0; x <= _caveEnd.x; x++)
            {
                for (int y = 0; y <= _caveEnd.y; y++)
                {
                    for (int z = 0; z <= _caveEnd.z; z++)
                    {
                        action(new Vector3Int(x, y, z));
                    }
                }
            }
        }

        private void IterateFloor(Action<Vector3Int> action)
        {
            for (int x = 0; x <= _caveEnd.x; x++)
            {
                for (int z = 0; z <= _caveEnd.z; z++)
                {
                    action(new Vector3Int(x, 0, z));
                }
            }
        }

        private float GetPerlinValue(Vector2 position)
        {
            return Mathf.PerlinNoise(position.x, position.y);
        }

        private bool[,,] GetSimplifiedCubesArray()
        {
            var result = new bool[_positionValues.GetLength(0), _positionValues.GetLength(0), _positionValues.GetLength(0)];
            IterateAllPositions(position =>
            {
                var value = GetValue(position);
                result[position.x, position.y, position.z] = value > _cubeThreshold;
            });

            return result;
        }

        private Vector3Int GetRandomGridPosition(Vector3Int min, Vector3Int max)
        {
            return new Vector3Int(_random.Next(min.x, max.x), _random.Next(min.y, max.y), _random.Next(min.z, max.z));
        }

        private void SetCirclePositions()
        {
            _circlePositions.Clear();
            for (int x = -_circleRadius; x < _circleRadius; x++)
            {
                for (int y = -_circleRadius; y < _circleRadius; y++)
                {
                    for (int z = -_circleRadius; z < _circleRadius; z++)
                    {
                        var distance = (Vector3.zero - new Vector3(x, y, z)).magnitude;
                        if (distance < _circleRadius)
                        {
                            _circlePositions.Add(new Vector3Int(x, y, z));
                        }
                    }
                }
            }
        }

        [Serializable]
        private class PerlinValueCubes
        {
            public List<PerlinValue> values;

            public int GetCubesForValue(float value)
            {
                for (int i = 0; i < values.Count; i++)
                {
                    if (value < values[i].value)
                    {
                        return values[i].cubes;
                    }
                }

                return 0;
            }

            [Serializable]
            public class PerlinValue
            {
                public float value;
                public int cubes;
            }
        }
    }
}