using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.Utils;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.MapGeneration
{

    public class CaveMeshCreator
    {
        private static readonly List<Vector3Int> CubeNeighbours = new List<Vector3Int>
        {
            Vector3Int.forward,
            Vector3Int.back,
            Vector3Int.up,
            Vector3Int.down,
            Vector3Int.left,
            Vector3Int.right,
        };

        private StructureGridService _structureGridService;
        private CaveCellTypeTextureData _caveCellTypeTextureData;

        private bool[,,] _cubes;
        private Vector3Int _cubesSize;
        private List<Quad> _quads;
        private List<Vector3> _veritces;
        private List<Vector3> _normals;
        private List<Vector2> _uvs;
        private List<int> _triangles;

        [Inject]
        public void Initialize(StructureGridService structureGridService, CaveCellTypeTextureData caveCellTypeTextureData)
        {
            _structureGridService = structureGridService;
            _caveCellTypeTextureData = caveCellTypeTextureData;
        }

        public Mesh CreateMesh(bool[,,] cubes)
        {
            _cubes = cubes;
            _cubesSize = new Vector3Int(_cubes.GetLength(0), _cubes.GetLength(1), _cubes.GetLength(2));
            _quads = new List<Quad>();
            _veritces = new List<Vector3>();
            _normals = new List<Vector3>();
            _uvs = new List<Vector2>();
            _triangles = new List<int>();
            CreateQuads();
            CalculateMesh();
            return SetMeshData();
        }

        private void CreateQuads()
        {
            _cubes.IterateAllPositions((x, y, z) =>
            {
                if (_cubes[x, y, z])
                {
                    foreach (var neighbour in CubeNeighbours)
                    {
                        if (!IsPositionInArray(x + neighbour.x, y + neighbour.y, z + neighbour.z))
                        {
                            continue;
                        }

                        if (!_cubes[x + neighbour.x, y + neighbour.y, z + neighbour.z])
                        {
                            _quads.Add(new Quad
                            {
                                Position = new Vector3Int(x, y, z),
                                FaceDirection = neighbour
                            });
                        }
                    }
                }
            });
        }

        private bool IsPositionInArray(int x, int y, int z)
        {
            if (x < 0 || y < 0 || z < 0)
            {
                return false;
            }

            if (x >= _cubesSize.x || y >= _cubesSize.y || z >= _cubesSize.z)
            {
                return false;
            }

            return true;
        }

        private void CalculateMesh()
        {
            foreach (var quad in _quads)
            {
                var vertices = quad.GetVertivesPositions();
                var indexes = new List<int>();
                foreach (var vertice in vertices)
                {
                    _veritces.Add(vertice);
                    var index = _veritces.Count - 1;
                    indexes.Add(index);
                }

                quad.SetVerticesIndexes(indexes);

                _triangles.AddRange(quad.GetTriangles());
                _normals.AddRange(quad.GetNormals());
                _uvs.AddRange(_caveCellTypeTextureData.GetUVsForType(_structureGridService.GetCellType(quad.Position)));
            }
        }

        private Mesh SetMeshData() => new()
        {
            name = "Cave",
            vertices = _veritces.ToArray(),
            triangles = _triangles.ToArray(),
            uv = _uvs.ToArray(),
            normals = _normals.ToArray(),
        };
    }
}