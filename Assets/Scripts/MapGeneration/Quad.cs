using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.MapGeneration
{
    public class Quad
    {
        public Vector3Int Position { get; set; }
        public Vector3Int FaceDirection { get; set; }

        private List<int> _verticesIndexes;

        public List<Vector3> GetVertivesPositions()
        {

            if (FaceDirection == Vector3Int.up)
            {
                return new List<Vector3>()
                {
                    Position + new Vector3(-0.5f,0.5f, -0.5f),
                    Position + new Vector3(-0.5f,0.5f, 0.5f),
                    Position + new Vector3(0.5f,0.5f, 0.5f),
                    Position + new Vector3(0.5f,0.5f, -0.5f),
                };
            }

            if (FaceDirection == Vector3Int.down)
            {
                return new List<Vector3>()
                {
                    Position + new Vector3(-0.5f, -0.5f, -0.5f),
                    Position + new Vector3(0.5f, -0.5f, -0.5f),
                    Position + new Vector3(0.5f, -0.5f, 0.5f),
                    Position + new Vector3(-0.5f, -0.5f, 0.5f),
                };
            }

            if (FaceDirection == Vector3Int.right)
            {
                return new List<Vector3>()
                {
                    Position + new Vector3(0.5f,-0.5f, -0.5f),
                    Position + new Vector3(0.5f,0.5f, -0.5f),
                    Position + new Vector3(0.5f,0.5f, 0.5f),
                    Position + new Vector3(0.5f,-0.5f, 0.5f),
                };
            }

            if (FaceDirection == Vector3Int.left)
            {
                return new List<Vector3>()
                {
                    Position + new Vector3(-0.5f,-0.5f, 0.5f),
                    Position + new Vector3(-0.5f,0.5f, 0.5f),
                    Position + new Vector3(-0.5f,0.5f, -0.5f),
                    Position + new Vector3(-0.5f,-0.5f, -0.5f),
                };
            }

            if (FaceDirection == Vector3Int.forward)
            {
                return new List<Vector3>()
                {
                    Position + new Vector3(0.5f, -0.5f, 0.5f),
                    Position + new Vector3(0.5f, 0.5f, 0.5f),
                    Position + new Vector3(-0.5f, 0.5f, 0.5f),
                    Position + new Vector3(-0.5f, -0.5f, 0.5f),
                };
            }

            if (FaceDirection == Vector3Int.back)
            {
                return new List<Vector3>()
                {
                    Position + new Vector3(-0.5f, -0.5f, -0.5f),
                    Position + new Vector3(-0.5f, 0.5f, -0.5f),
                    Position + new Vector3(0.5f, 0.5f, -0.5f),
                    Position + new Vector3(0.5f,-0.5f, -0.5f),
                };
            }

            throw new NotSupportedException();
        }

        public void SetVerticesIndexes(List<int> indexes)
        {
            _verticesIndexes = indexes;
        }

        public List<int> GetTriangles()
        {
            return new List<int>
            {
                _verticesIndexes[0],
                _verticesIndexes[1],
                _verticesIndexes[2],
                _verticesIndexes[0],
                _verticesIndexes[2],
                _verticesIndexes[3],
            };
        }

        public List<Vector3> GetNormals()
        {
            return new List<Vector3>()
            {
                FaceDirection,
                FaceDirection,
                FaceDirection,
                FaceDirection
            };
        }
    }
}