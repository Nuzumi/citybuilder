using Assets.Scripts.Ui;

namespace Assets.Scripts.Utils.Attributes
{
    public class PrefabAttribute : System.Attribute
    {
        public UiELementLayer PopupLayer { get; }
        public bool AllowMultipleElements { get; }

        public PrefabAttribute(UiELementLayer layer, bool allwMultipleElements)
        {
            PopupLayer = layer;
            AllowMultipleElements = allwMultipleElements;
        }
    }
}