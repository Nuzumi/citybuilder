using Assets.Scripts.BuildingElements;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Utils
{
    public static class Extensions
    {
        private static readonly List<Vector3Int> PositionNeighbours2D = new()
        {
            Vector3Int.right,
            Vector3Int.left,
            Vector3Int.forward,
            Vector3Int.back
        };

        private static readonly List<Vector3Int> PositionNeighbours3D = new(PositionNeighbours2D)
        {
            Vector3Int.up,
            Vector3Int.down
        };

        public static Vector3Int ToIntVector(this Vector3 vector)
        {
            return new Vector3Int(ConvertFloatToInt(vector.x), ConvertFloatToInt(vector.y), ConvertFloatToInt(vector.z));
        }

        private static int ConvertFloatToInt(float v) => Mathf.RoundToInt(v);

        public static Vector3Int RotateVector(this Vector3Int position, Quaternion rotation, Vector3Int pivotPosition)
        {
            var translated = position - pivotPosition;
            var rotated = rotation * translated;
            var translatedBack = rotated + pivotPosition;
            return translatedBack.ToIntVector();
        }

        public static List<Vector3Int> GetNeighbours(this Vector3Int position)
        {
            var result = new List<Vector3Int>();

            foreach (var neighbour in PositionNeighbours2D)
            {
                result.Add(position + neighbour);
            }

            return result;
        }

        public static List<Vector3Int> GetNeighbours3D(this Vector3Int position)
        {
            var result = new List<Vector3Int>();

            foreach (var neighbour in PositionNeighbours3D)
            {
                result.Add(position + neighbour);
            }

            return result;
        }

        public static int GetManchattanDistance(this Vector3Int from, Vector3Int to)
        {
            return Mathf.Abs(from.x - to.x) +
                Mathf.Abs(from.y - to.y) +
                Mathf.Abs(from.z - to.z);
        }

        public static ResourceData GetNegative(this ResourceData resourceData)
        {
            return new ResourceData()
            {
                type = resourceData.type,
                amount = -resourceData.amount
            };
        }

        public static void IterateAllPositions<T>(this T[,,] data, Action<int, int, int> action)
        {
            for (int x = 0; x < data.GetLength(0); x++)
            {
                for (int y = 0; y < data.GetLength(1); y++)
                {
                    for (int z = 0; z < data.GetLength(2); z++)
                    {
                        action(x, y, z);
                    }
                }
            }
        }

        public static bool IsInArray<T>(this T[,,] data, Vector3Int position) => IsInArray(data, position.x, position.y, position.z);

        public static bool IsInArray<T>(this T[,,] data, int x, int y, int z)
        {
            if (x < 0 || y < 0 || z < 0)
            {
                return false;
            }
            if (x >= data.GetLength(0) || y >= data.GetLength(1) || z >= data.GetLength(2))
            {
                return false;
            }

            return true;
        }
    }
}