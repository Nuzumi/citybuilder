using System;

namespace Assets.Scripts.Utils
{
    public class UpdatableProperty<T>
    {
        public bool CanBeUpdated { get; }
        public event Action ValueUpdated;

        public T Value
        {
            get => _value;
            set
            {
                if (!CanBeUpdated)
                {
                    return;
                }

                _value = value;
                ValueUpdated?.Invoke();
            }
        }

        private T _value;

        public UpdatableProperty(bool canBeUpdated = true, T value = default)
        {
            CanBeUpdated = canBeUpdated;
            _value = value;
        }
    }
}