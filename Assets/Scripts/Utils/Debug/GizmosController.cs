using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Utils.Debug
{
    public class DebugGizmo
    {
        public Color color;
        public Vector3 position;
        public float radius;
    }

    public class GizmosController : MonoBehaviour
    {
        private static GizmosController _instance;

        private List<DebugGizmo> _gizmos = new();

        private void Awake()
        {
            _instance = this;
        }

        public static void AddGizmoToDraw(DebugGizmo gizmo) => _instance.AddGizmo(gizmo);

        public static void RemoveGizmoToDraw(DebugGizmo gizmo) => _instance.RemoveGizmo(gizmo);

        public static void ClearGizmosToDraw() => _instance.ClearGizmos();

        private void AddGizmo(DebugGizmo gizmo) => _gizmos.Add(gizmo);

        private void RemoveGizmo(DebugGizmo gizmo) => _gizmos.Remove(gizmo);

        private void ClearGizmos() => _gizmos.Clear();

        private void OnDrawGizmosSelected()
        {
            foreach (var gizmo in _gizmos)
            {
                Gizmos.color = gizmo.color;
                Gizmos.DrawSphere(gizmo.position, gizmo.radius);
            }
        }
    }
}