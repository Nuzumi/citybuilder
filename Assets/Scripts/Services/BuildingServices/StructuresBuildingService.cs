using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.BuildingElements;
using System;
using Zenject;
using static Assets.Scripts.Services.BuildingServices.StructuresPrefabProvider;
using Assets.Scripts.Services.StructuresServices;
using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.StructureGrid;

namespace Assets.Scripts.Services.BuildingServices
{

    public class StructuresBuildingService : IInitializable
    {
        public event Action BuildFinished;
        public event Action<bool> BuildingModeActive;

        public bool IsInBuildingMode { get; private set; }
        public StructureBuildingElement LasBuildElement { get; private set; }

        private readonly StructuresFactory _structuresFactory;
        private readonly BuildingInputService _buildingInputService;
        private readonly ResourceService _resourceService;
        private readonly StructuresService _structuresService;
        private readonly StructureGridService _structureGridService;

        private StructureBuildingElement _currentBuildingElement;
        private StructureBuildingPrefabData _currentBuildingPrefabData;

        public StructuresBuildingService(
            StructuresFactory structuresFactory,
            BuildingInputService buildingInputService,
            ResourceService resourceService,
            StructuresService structuresService,
            StructureGridService structureGridService)
        {
            _structuresFactory = structuresFactory;
            _buildingInputService = buildingInputService;
            _resourceService = resourceService;
            _structuresService = structuresService;
            _structureGridService = structureGridService;
        }

        public void StartBuildingMode(StructureBuildingPrefabData buildingPrefabData)
        {
            if (IsInBuildingMode)
            {
                return;
            }

            if (buildingPrefabData.prefab.CanBeAtatchedToTakenCell)
            {
                _structureGridService.ShowGridForAtatchedBuilding();
            }
            else
            {
                _structureGridService.ShowGrid();
            }

            _buildingInputService.EnableBuildingMode();
            BuildingModeActive?.Invoke(true);
            IsInBuildingMode = true;

            _currentBuildingPrefabData = buildingPrefabData;
            _currentBuildingElement = _structuresFactory.SpawnBuildingElement(buildingPrefabData);
        }

        public void Initialize()
        {
            _buildingInputService.AcceptBuilding += OnBuildingAccepted;
            _buildingInputService.CancelBuilding += OnBuildingCanceled;
            _buildingInputService.Rotate += OnBuildingRotated;
            _structureGridService.GridCellSelected += OnGridCellSelected;
        }

        private void OnBuildingAccepted()
        {
            if (!IsInBuildingMode)
            {
                return;
            }

            if (!CanBuildStructure())
            {
                return;
            }

            LasBuildElement = _currentBuildingElement;
            _structuresService.RegisterStructure(_currentBuildingElement, _currentBuildingPrefabData);

            _structuresFactory.CompleteBuild();
            _currentBuildingElement.FinishBuild(_currentBuildingPrefabData.id);

            _structureGridService.HideGrid();

            var takenCells = _currentBuildingElement.TakenCells;
            _structureGridService.AddTakenGridCells(takenCells.cells, takenCells.type);
            var requiredCells = _currentBuildingElement.RequiredCells;
            _structureGridService.AddTakenGridCells(requiredCells.cells, requiredCells.type);

            BuildingModeActive?.Invoke(false);
            IsInBuildingMode = false;
            BuildFinished?.Invoke();
        }

        private void OnBuildingCanceled()
        {
            if (!IsInBuildingMode)
            {
                return;
            }

            BuildingModeActive?.Invoke(false);
            IsInBuildingMode = false;
            _structureGridService.HideGrid();
            UnityEngine.Object.Destroy(_currentBuildingElement.gameObject);
        }

        private void OnBuildingRotated()
        {
            if (!IsInBuildingMode)
            {
                return;
            }

            _currentBuildingElement.Rotate();
        }

        private bool CanBuildStructure()
        {
            return _currentBuildingElement.CanBeAtatchedToTakenCell ? CanBuildingFitOnWallGrid() : CanBuildingFitOnGrid();
        }

        private bool CanBuildingFitOnGrid()
        {
            var requiredCells = _currentBuildingElement.RequiredCells;
            if (!_structureGridService.AreGridCellsFree(requiredCells.cells, requiredCells.type))
            {
                return false;
            }

            return !_structureGridService.AreGridCellsTaken(_currentBuildingElement.TakenCells.cells);
        }

        private bool CanBuildingFitOnWallGrid()
        {
            var requiredCells = _currentBuildingElement.RequiredCells;
            if (!_structureGridService.AreWallGridCellsFree(requiredCells.cells, requiredCells.type))
            {
                return false;
            }

            return !_structureGridService.AreGridCellsTaken(_currentBuildingElement.TakenCells.cells);
        }

        private void OnGridCellSelected(IStructureGridCell cell)
        {
            if (!IsInBuildingMode)
            {
                return;
            }

            _currentBuildingElement.StartPlacementOnPosition(cell.GridPosition);
        }
    }
}