﻿using Assets.Scripts.BuildingElements;
using System;
using UnityEngine;
using Zenject;
using static Assets.Scripts.Services.BuildingServices.StructuresPrefabProvider;

namespace Assets.Scripts.Services.BuildingServices
{
    public class StructuresFactory
    {
        private readonly DiContainer _container;
        private readonly Transform _structuresParent;
        private GameObject _currentBuilding;

        public StructuresFactory(DiContainer container, Transform structuresParent)
        {
            _container = container;
            _structuresParent = structuresParent;
        }

        public StructureBuildingElement SpawnBuildingElement(StructureBuildingPrefabData prefabData)
        {
            var element = UnityEngine.Object.Instantiate(prefabData.prefab, _structuresParent);
            _currentBuilding = element.gameObject;
            return element;
        }

        public void CompleteBuild()
        {
            _container.InjectGameObject(_currentBuilding);
            _currentBuilding = null;
        }
    }
}