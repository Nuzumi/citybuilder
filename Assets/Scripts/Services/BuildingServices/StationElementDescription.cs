﻿using Assets.Scripts.BuildingElements;
using Assets.Scripts.Ui.StructureTooltip;
using System.Collections.Generic;

namespace Assets.Scripts.Services.BuildingServices
{
    public class StructureDescription
    {
        public int StructureId { get; set; }
        public StructureStateType StructureState { get; set; }
        public StructureTooltipType StructureTooltipType { get; set; }

        public List<ResourceData> BuildingCost { get; } = new();
        public List<ResourceData> StoredResources { get; } = new ();
        public List<ResourceData> MaxStoredResources { get; } = new ();
        public List<ResourceData> GeneratedResources { get; } = new();
        public List<ResourceData> CurrentGeneratingResource { get; } = new();
        public int CurrentGenerationCycle { get; set; }
        public int CyclesToCreateResource { get; set; }
        public int GeneralStorageCapacity { get; set; }
    }
}