﻿using Assets.Scripts.BuildingElements;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Services.BuildingServices
{
    [CreateAssetMenu(menuName = "Data/" + nameof(StructuresPrefabProvider), fileName = nameof(StructuresPrefabProvider))]
    public class StructuresPrefabProvider : ScriptableObject
    {
        public List<StructureBuildingPrefabData> PrefabsDataToDisplay => prefabsData.Where(pd => !pd.hideInShop).ToList();

        public List<StructureBuildingPrefabData> prefabsData;

        public StructureBuildingPrefabData GetStructureData(int id) => prefabsData.Find(pd => pd.id == id);

#if UNITY_EDITOR
        private void OnEnable()
        {
            for (int i = 0; i < prefabsData.Count; i++)
            {
                prefabsData[i].id = i;
            }
        }
#endif

        [Serializable]
        public class StructureBuildingPrefabData
        {
            public string name;
            public bool hideInShop;
            public int id;
            public StructureBuildingElement prefab;
        }
    }
}