using System;

namespace Assets.Scripts.Services.BuildingServices
{
    public class BuildingInputService
    {
        public event Action AcceptBuilding;
        public event Action CancelBuilding;
        public event Action Rotate;

        private InputActions.BuildModeControllActions _buildControlls;

        public BuildingInputService()
        {
            _buildControlls = new InputActions().BuildModeControll;
            Subscribe();
        }

        public void EnableBuildingMode() => _buildControlls.Enable();

        public void DisableBuildingMode() => _buildControlls.Disable();

        private void Subscribe()
        {
            _buildControlls.Accept.performed += _ => AcceptBuilding?.Invoke();
            _buildControlls.Cancle.performed += _ => CancelBuilding?.Invoke();
            _buildControlls.Rotate.performed += _ => Rotate?.Invoke();
        }
    }
}