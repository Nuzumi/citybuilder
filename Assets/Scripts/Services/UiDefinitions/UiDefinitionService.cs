using Assets.Scripts.Ui.MainMenu;
using Assets.Scripts.Ui.StructuresDisplay;
using Assets.Scripts.Ui.StructureTooltip;
using Services.UI;
using Zenject;

namespace Assets.Scripts.Services.UiDefinitions
{
    public class UiDefinitionService : IInitializable
    {
        private readonly ViewsService _viewsService;

        public UiDefinitionService(ViewsService viewsService)
        {
            _viewsService = viewsService;
        }

        public void Initialize()
        {
            _viewsService.ShowUiElement<StructuresDisplay>();
            _viewsService.ShowUiElement<MainMenuDisplay>();
            _viewsService.ShowUiElement<ResourcesDisplay>();
            _viewsService.ShowUiElement<StructureTooltipDisplay>();
        }
    }
}