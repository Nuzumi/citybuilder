﻿using System.IO;
using UnityEngine;

namespace Assets.Scripts.Services.SaveLoadServices
{
    public static class SaveDataPaths
    {
        private const string SaveGameStateData = "gameStateData";
        private const string SaveInitialGameStateData = "initialGameStateData.json";

        public static string GetSaveGameStateDataPath() => Path.Combine(Application.persistentDataPath, SaveGameStateData);
        public static string GetInitialGameStateDataPath() => Path.Combine(Application.streamingAssetsPath, SaveInitialGameStateData);
    }
}