using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.Services.BuildingServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Zenject;
using Assets.Scripts.Services.StructuresServices;
using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.BuildingElements;
using Assets.Scripts.Services.CaveServices;
using Newtonsoft.Json;

using Debug = UnityEngine.Debug;

namespace Assets.Scripts.Services.SaveLoadServices
{

    public class SaveService : IInitializable, IDisposable
    {
        private readonly ResourceService _resourceService;
        private readonly ResourceUpdateTimer _updateTimer;
        private readonly StructuresService _structuresService;
        private readonly StructuresBuildingService _buildingService;
        private readonly StructureGridService _structureGridService;
        private readonly LoadService _loadService;
        private readonly CaveService _caveService;

        private JsonSerializerSettings _jsonSerializerSettings;

        public SaveService(
            ResourceService resourceService,
            ResourceUpdateTimer updateTimer,
            StructuresService structuresService,
            StructuresBuildingService buildingService,
            StructureGridService structureGridService,
            LoadService loadService, 
            CaveService caveService)
        {
            _resourceService = resourceService;
            _updateTimer = updateTimer;
            _structuresService = structuresService;
            _buildingService = buildingService;
            _structureGridService = structureGridService;
            _loadService = loadService;
            _caveService = caveService;
        }

        public void Initialize()
        {
            _jsonSerializerSettings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            };

            _buildingService.BuildFinished += SaveGameState;
            _updateTimer.ResourceUpdate += SaveGameState;
        }

        public void Dispose()
        {
            _buildingService.BuildFinished -= SaveGameState;
            _updateTimer.ResourceUpdate -= SaveGameState;
        }

        private void SaveGameState()
        {
            if (!_loadService.IsGameLoaded)
            {
                return;
            }

            var gameState = GetGameState();
            var saveDataPath = SaveDataPaths.GetSaveGameStateDataPath();

            Task.Run(() =>
            {
                try
                {
                    var gameStateJson = JsonConvert.SerializeObject(gameState, Formatting.None, _jsonSerializerSettings);
                    File.WriteAllText(saveDataPath, gameStateJson);
                }
                catch (Exception ex)
                {
                    Debug.LogException(ex);
                }
            });
        }

        private GameState GetGameState()
        {
            return new GameState()
            {
                resources = GetResourcesData(),
                takenGridTiles = _structureGridService.TakenGridPositions,
                cellsType = _structureGridService.CellTypes,
                structuresSaveDatas = GetStructuresData(),
                caveData = _caveService.GetCaveData(),
            };
        }

        private List<ResourceData> GetResourcesData()
        {
            return _resourceService.CityResources.Select(resource => new ResourceData() { type = resource.Type, amount = resource.Amount.Value }).ToList();
        }

        private List<StructureSaveData> GetStructuresData()
        {
            var result = new List<StructureSaveData>();
            foreach (var (buildingElement, prefabData) in _structuresService.StructureData)
            {
                result.Add(new StructureSaveData
                {
                    id = prefabData.id,
                    elementPosition = buildingElement.transform.position,
                    elementRotation = buildingElement.transform.rotation,
                    structureSaveState = buildingElement.GetStructureSaveState()
                });
            }

            return result;
        }
    }
}