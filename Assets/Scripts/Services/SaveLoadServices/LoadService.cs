﻿using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.Services.BuildingServices;
using System;
using System.IO;
using UnityEngine;
using Assets.Scripts.Services.StructuresServices;
using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.Services.CaveServices;
using Newtonsoft.Json;

namespace Assets.Scripts.Services.SaveLoadServices
{
    public class LoadService
    {
        public event Action GameLoaded;

        public bool IsGameLoaded { get; private set; }

        private readonly StructuresPrefabProvider _prefabProvider;
        private readonly StructuresService _structuresService;
        private readonly ResourceService _resourceService;
        private readonly StructuresFactory _elementsFactory;
        private readonly StructureGridService _structureGridService;
        private readonly CaveService _caveService;

        public LoadService(
            StructuresPrefabProvider prefabProvider,
            StructuresService structuresService,
            ResourceService resourceService,
            StructuresFactory elementsFactory,
            StructureGridService structureGridService,
            CaveService caveService)
        {
            _prefabProvider = prefabProvider;
            _structuresService = structuresService;
            _resourceService = resourceService;
            _elementsFactory = elementsFactory;
            _structureGridService = structureGridService;
            _caveService = caveService;
        }

        public void LoadSavedData()
        {
            LoadData(LoadGameState(SaveDataPaths.GetSaveGameStateDataPath()));
        }

        public void LoadInitialSaveData()
        {
            LoadData(LoadGameState(SaveDataPaths.GetInitialGameStateDataPath()));
        }

        private void LoadData(GameState gameState)
        {
            InitializeLoadedData(gameState);
            GameLoaded?.Invoke();
        }

        public void SetGameEnded() => IsGameLoaded = false;

        private void InitializeLoadedData(GameState gameState)
        {
            SetResourcesValues(gameState);
            SetTiles(gameState);
            CreateStructure(gameState);
            SetCaveData(gameState);
            IsGameLoaded = true;
        }

        private GameState LoadGameState(string path)
        {
            try
            {
                var laodedText = File.ReadAllText(path);
                return JsonConvert.DeserializeObject<GameState>(laodedText);
            }
            catch(Exception ex)
            {
                Debug.LogException(ex);
                return null;
            }
        }

        private void SetCaveData(GameState gameState)
        {
            _caveService.SetCaveData(gameState.caveData);
        }

        private void SetResourcesValues(GameState gameState)
        {
            foreach (var resource in gameState.resources)
            {
                var cityResource = _resourceService.GetResource(resource.type);
                if(cityResource != null)
                {
                    cityResource.Amount.Value = resource.amount;
                }
            }
        }

        private void SetTiles(GameState gameState)
        {
            _structureGridService.AddTakenGridCells(gameState.takenGridTiles);
            _structureGridService.SetCellsType(gameState.cellsType);
        }

        private void CreateStructure(GameState gameState)
        {
            foreach (var elementSaveData in gameState.structuresSaveDatas)
            {
                CreateStructure(elementSaveData);
            }
        }

        private void CreateStructure(StructureSaveData elementSaveData)
        {
            var prefabData = _prefabProvider.prefabsData.Find(pd => pd.id == elementSaveData.id);
            var spawnedElement = _elementsFactory.SpawnBuildingElement(prefabData);
            spawnedElement.transform.SetPositionAndRotation(elementSaveData.elementPosition, elementSaveData.elementRotation);
            _structuresService.RegisterStructure(spawnedElement, prefabData);
            _elementsFactory.CompleteBuild();
            spawnedElement.LoadStructureSaveState(elementSaveData.structureSaveState);
        }
    }
}