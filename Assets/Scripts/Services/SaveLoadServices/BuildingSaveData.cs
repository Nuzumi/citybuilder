﻿using Assets.Scripts.BuildingElements;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Services.SaveLoadServices
{
    [Serializable]
    public class StructureSaveData
    {
        public int id;
        public Vector3 elementPosition;
        public Quaternion elementRotation;
        public StructureSaveState structureSaveState;
    }

    [Serializable]
    public class StructureSaveState
    {
        public int structureId;
        public StructureStateType structureStateType;
        public List<ResourceData> storedResources;
        public bool isGenerating;
        public int generationCycle;
    }
}