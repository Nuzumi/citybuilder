﻿using Assets.Scripts.BuildingElements;
using Assets.Scripts.Services.StructureGridServices;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Services.SaveLoadServices
{
    public class GameState
    {
        public List<ResourceData> resources;
        public List<Vector3Int> takenGridTiles;
        public List<StructureSaveData> structuresSaveDatas;
        public bool[,,] caveData;
        public Dictionary<Vector3Int, GridCellType> cellsType;
    }
}