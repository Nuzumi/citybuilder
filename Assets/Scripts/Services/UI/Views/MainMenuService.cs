using Assets.Scripts.Services.SaveLoadServices;
using Assets.Scripts.Ui.MainMenu;
using Services.UI;

namespace Assets.Scripts.Services.UI.Views
{
    public class MainMenuService
    {
        private readonly ViewsService _viewsService;
        private readonly LoadService _loadService;

        public MainMenuService(ViewsService viewsService, LoadService loadService)
        {
            _viewsService = viewsService;
            _loadService = loadService;
        }

        public void LoadStationData()
        {
            _loadService.LoadSavedData();
            _viewsService.CloseUiElement<MainMenuDisplay>();
        }

        public void LoadInitialData()
        {
            _loadService.LoadInitialSaveData();
            _viewsService.CloseUiElement<MainMenuDisplay>();
        }
    }
}