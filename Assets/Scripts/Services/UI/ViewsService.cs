using Assets.Scripts.Ui;
using Assets.Scripts.Utils.Attributes;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Services.UI
{
    public class ViewsService
    {
        private readonly PopupsFactory _popupsCreatorService;
        private readonly UiElementsLayerData _popupsLayerData;
        private readonly Dictionary<UiELementLayer, Dictionary<Type,GameObject>> _popupsAtLayers = new ();

        public ViewsService(PopupsFactory popupsCreatorService, UiElementsLayerData popupsLayerData)
        {
            _popupsCreatorService = popupsCreatorService;
            _popupsLayerData = popupsLayerData;
        }

        public void ShowUiElement<T>() where T : UiElement => ShowUiElement(typeof(T));
        public void ShowUiElement<T,D>(D data) where T : UiElement => ShowUiElement(typeof(T), data);

        public void CloseUiElement<T>() where T : UiElement => DestroyUiElement(typeof(T));

        private void ShowUiElement<D>(Type uiElement, D data)
        {
            var popup = ShowUiElement(uiElement);

            if (popup.TryGetComponent<IDataInjectable<D>>(out var dataInjectable))
            {
                dataInjectable.InjectData(data);
            }
        }

        private GameObject ShowUiElement(Type uiElementType)
        {
            var attribute = GetPrefabAttribute(uiElementType);
            var popup = _popupsCreatorService.CreatePopup(uiElementType, _popupsLayerData.GetLayerTransform(attribute.PopupLayer));
            popup.GetComponent<UiElement>().Destroyed += () => _popupsAtLayers[attribute.PopupLayer].Remove(uiElementType);

            if (!_popupsAtLayers.ContainsKey(attribute.PopupLayer))
            {
                _popupsAtLayers.Add(attribute.PopupLayer, new Dictionary<Type, GameObject>());
            }

            if (!attribute.AllowMultipleElements)
            {
                foreach (var item in _popupsAtLayers[attribute.PopupLayer].Values)
                {
                    UnityEngine.Object.Destroy(item);
                }
            }

            _popupsAtLayers[attribute.PopupLayer][uiElementType] = popup;
            return popup;
        }

        private void DestroyUiElement(Type type)
        {
            var layer = GetPrefabAttribute(type).PopupLayer;
            
            if (!_popupsAtLayers.ContainsKey(layer))
            {
                return;
            }

            if (!_popupsAtLayers[layer].ContainsKey(type))
            {
                return;
            }

            var toDestroy = _popupsAtLayers[layer][type];
            _popupsAtLayers[layer].Remove(type);
            UnityEngine.Object.Destroy(toDestroy);
        }

        private static PrefabAttribute GetPrefabAttribute(Type uiElementType) 
            => (PrefabAttribute)Attribute.GetCustomAttribute(uiElementType, typeof(PrefabAttribute));
    }
}
