﻿using Assets.Scripts.Units;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Services.UnitMovementServices
{
    public class UnitMoveData
    {
        public int priority;
        public Vector3Int startPosition;
        public Action dataChosenAction;
        public List<(UnitIntrestPoint, Action<Unit>)> unitIntrestPoints;
    }
}