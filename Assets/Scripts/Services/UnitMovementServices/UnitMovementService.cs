using Assets.Scripts.BuildingElements;
using Assets.Scripts.Services.PathServices;
using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.Units;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Services.UnitMovementServices
{

    public class UnitMovementService : IInitializable
    {
        private readonly Dictionary<UnitIntrestPointType, List<UnitIntrestPoint>> _typePoints = new();
        private readonly List<Unit> _freeUnits = new ();
        private readonly List<Unit> _busyUnits = new ();
        private readonly List<UnitMoveData> _moveDatas = new();

        private ResourceUpdateTimer _updateTimer;
        private readonly PathService _pathService;

        public UnitMovementService(ResourceUpdateTimer timer, PathService pathService)
        {
            _updateTimer = timer;
            _pathService = pathService;
        }

        public void Initialize()
        {
            _updateTimer.ResourceUpdate += OnUpdate;
            _pathService.PathServiceInitialized += TryStartUnitMove;
        }

        public void RegisterIntrestPoint(UnitIntrestPoint intrestPoint)
        {
            if (!_typePoints.ContainsKey(intrestPoint.Type))
            {
                _typePoints.Add(intrestPoint.Type, new List<UnitIntrestPoint>());
            }
            _typePoints[intrestPoint.Type].Add(intrestPoint);

            TryStartUnitMove();
        }

        public void RemoveIntrestPoint(UnitIntrestPoint intrestPoint)
        {
            if (!_typePoints.ContainsKey(intrestPoint.Type))
            {
                return;
            }

            _typePoints[intrestPoint.Type].Remove(intrestPoint);
        }

        public void AddFreeUnit(Unit unit)
        {
            _freeUnits.Add(unit);
            _busyUnits.Remove(unit);

            TryStartUnitMove();
        }

        private void OnUpdate()
        {
            if(_freeUnits.Count == 0)
            {
                return;
            }

            TryStartUnitMove();
        }

        private void TryStartUnitMove()
        {
            if (!_pathService.IsInitialized)
            {
                return;
            }

            CalculateMovesData();
            if (_moveDatas.Count != 0 && _freeUnits.Count != 0)
            {
                StartUnitMove();
            }
        }

        private void StartUnitMove()
        {
            var highestPriority = _moveDatas.Max(data => data.priority);
            var highestPriorityMoves = _moveDatas.Where(data => data.priority == highestPriority).ToList();

            var unit = _freeUnits[0];
            _freeUnits.RemoveAt(0);
            _busyUnits.Add(unit);

            var moveData = highestPriorityMoves
                .Select(move => (move, move.startPosition.GetManchattanDistance(unit.Position)))
                .OrderBy(data => data.Item2)
                .Select(data => data.move)
                .FirstOrDefault();

            _moveDatas.Remove(moveData);

            moveData.dataChosenAction();
            moveData.dataChosenAction = null;

            unit.StartMove(moveData);
        }

        private void CalculateMovesData()
        {
            _moveDatas.Clear();

            AddConsumersMoveData();
            AddGeneratorMoveData();
        }

        private void AddConsumersMoveData()
        {
            if (!_typePoints.ContainsKey(UnitIntrestPointType.Consumer))
            {
                return;
            }

            foreach (var consumer in _typePoints[UnitIntrestPointType.Consumer])
            {
                if (consumer.IsFull())
                {
                    return;
                }

                var consumerResources = consumer.GetResources();
                consumerResources.ForEach(resource => resource.amount = 1);
                var neededConsumerResources = consumerResources.Where(resource => consumer.CanStoreResource(resource)).ToList();

                foreach (var neededResource in neededConsumerResources)
                {
                    var closestStorage = GetClosestStorage(consumer.Position, storage => storage.CanRemoveResource(neededResource));

                    if (closestStorage == null)
                    {
                        continue;
                    }

                    var priority = GetIntrestPointPriority(consumer, neededResource);
                    AddMoveData(closestStorage, consumer, neededResource, priority);
                }
            }
        }

        private void AddGeneratorMoveData()
        {
            if (!_typePoints.ContainsKey(UnitIntrestPointType.Generator))
            {
                return;
            }

            foreach (var generator in _typePoints[UnitIntrestPointType.Generator])
            {
                if (generator.IsEmpty())
                {
                    continue;
                }

                var generatedResources = generator.GetResources();
                generatedResources.ForEach(resource => resource.amount = 1);

                foreach(var generatedResource in generatedResources)
                {
                    var closestStorage = GetClosestStorage(generator.Position, storage => storage.CanStoreResource(generatedResource));

                    if(closestStorage == null)
                    {
                        continue;
                    }

                    var priority = GetIntrestPointPriority(generator, generatedResource);
                    AddMoveData(generator, closestStorage, generatedResource, priority);
                }
            }
        }

        private void AddMoveData(UnitIntrestPoint start, UnitIntrestPoint finish, ResourceData resourceToMove, int priority)
        {
            var moveData = new UnitMoveData
            {
                priority = priority,
                startPosition = start.Position,
                unitIntrestPoints = new List<(UnitIntrestPoint, Action<Unit>)>(),
                dataChosenAction = () => start.ScheduleResourceChange(resourceToMove.GetNegative())
            };

            var resourceType = resourceToMove.type;
            var resourceAmount = resourceToMove.amount;

            moveData.unitIntrestPoints.Add((start, unit =>
            {
                var resource = new ResourceData()
                {
                    amount = resourceAmount,
                    type = resourceType
                };

                if (start.CanRemoveResource(resource))
                {
                    start.RemoveResource(resource);
                    start.ScheduleResourceChange(resource);
                }
                else
                {
                    unit.StopMove();
                }
            }
            ));

            moveData.unitIntrestPoints.Add((finish, unit =>
            {
                var resource = new ResourceData()
                {
                    amount = resourceAmount,
                    type = resourceType
                };

                finish.StoreResource(resource);
            }
            ));

            _moveDatas.Add(moveData);
        }

        private UnitIntrestPoint GetClosestStorage(Vector3Int position, Predicate<UnitIntrestPoint> predicate)
        {
            if (!_typePoints.ContainsKey(UnitIntrestPointType.Storage))
            {
                return null;
            }

            return _typePoints[UnitIntrestPointType.Storage]
                        .Where(storage => predicate(storage))
                        .Where(storage => _pathService.DoesPathExist(position, storage.Position))
                        .Select(storage => (storage, storage.Position.GetManchattanDistance(position)))
                        .OrderBy(data => data.Item2)
                        .Select(data => data.storage)
                        .FirstOrDefault();
        }

        private int GetIntrestPointPriority(UnitIntrestPoint intrestPoint, ResourceData resourceData)
        {
            const int HighPriority = 10;
            const int InsufficientFillAmountPriority = 5;

            var priority = 0;

            if(intrestPoint.Type == UnitIntrestPointType.Generator && intrestPoint.IsFull())
            {
                priority += HighPriority;
            }

            if (intrestPoint.Type == UnitIntrestPointType.Consumer && intrestPoint.IsEmpty())
            {
                priority += HighPriority;
            }

            var fillRate = intrestPoint.GetFillRate(resourceData.type);
            if(intrestPoint.Type == UnitIntrestPointType.Generator)
            {
                fillRate = 1 - fillRate;
            }

            if(fillRate <= .5f)
            {
                priority += InsufficientFillAmountPriority;
            }

            return priority;
        }
    }
}