﻿using Assets.Scripts.BuildingElements;
using Assets.Scripts.Services.ResourcesServices;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Services.UnitMovementServices
{
    public class UnitIntrestPoint
    {
        public UnitIntrestPointType Type { get; }
        public Vector3Int Position { get; }

        private readonly IStructureStorage _storage;
        private readonly List<ResourceData> _scheduledResourceDeliveries = new ();

        public UnitIntrestPoint(UnitIntrestPointType type, Vector3Int position, IStructureStorage storage)
        {
            Type = type;
            Position = position;
            _storage = storage;
        }

        public List<ResourceData> GetResources() => CombineResources(_storage.GetAllResources(), _scheduledResourceDeliveries);

        public bool IsEmpty()
        {
            return GetResources().All(resource => resource.amount == 0);
        }

        public bool IsFull()
        {
            var resources = GetResources();
            if (!resources.Any())
            {
                return false;
            }

            return resources.All(resource => _storage.MaxStorage[resource.type] == resource.amount);
        }

        public bool CanStoreResource(ResourceData resource) => _storage.CanStoreResource(resource);

        public void StoreResource(ResourceData resource) => _storage.StoreResource(resource);

        public bool CanRemoveResource(ResourceData resource)
        {
            if (!_storage.MaxStorage.ContainsKey(resource.type))
            {
                return false;
            }

            var resources = CombineResources(_storage.GetAllResources(), _scheduledResourceDeliveries);
            var storedResource = resources.Find(r => r.type == resource.type);

            if(storedResource == null)
            {
                return false;
            }

            return storedResource.amount - resource.amount >= 0;
        }

        public void RemoveResource(ResourceData resource) => _storage.RemoveResource(resource);

        public void ScheduleResourceChange(ResourceData resource) => AddResource(_scheduledResourceDeliveries, resource);

        public float GetFillRate(ResourceType resourceType)
        {
            var maxFill = _storage.MaxStorage[resourceType];
            var resource = GetResources().Find(r => r.type == resourceType);

            return resource == null ? 0 : resource.amount / (float)maxFill;
        }

        private List<ResourceData> CombineResources(List<ResourceData> resources, List<ResourceData> resourcesToAdd)
        {
            foreach (var resource in resourcesToAdd)
            {
                AddResource(resources, resource);
            }

            return resources;
        }

        private void AddResource(List<ResourceData> resources, ResourceData resourceToAdd)
        {
            var resource = resources.Find(r => r.type == resourceToAdd.type);

            if(resource != null)
            {
                resource.amount += resourceToAdd.amount;
                if(resource.amount <= 0)
                {
                    resources.Remove(resource);
                }
            }
            else
            {
                resources.Add(resourceToAdd);
            }
        }
    }
}