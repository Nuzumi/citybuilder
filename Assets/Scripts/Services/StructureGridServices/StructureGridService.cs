using Assets.Scripts.StructureGrid;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Services.StructureGridServices
{

    public class StructureGridService : IInitializable
    {
        private const int InitialGridCellsAmount = 2500;
        private const int InitialWallGridCellsAmount = 10000;

        public event Action<IStructureGridCell> GridCellSelected;
        public List<Vector3Int> TakenGridPositions => _takenGridPositions.ToList();
        public Dictionary<Vector3Int, GridCellType> CellTypes { get; private set; } = new();

        private StructureGridCell _cellPrefab;
        private WallStructureGridCell _wallCellPrefab;
        private List<StructureGridCell> _gridCells = new ();
        private List<WallStructureGridCell> _wallGridCells = new ();
        private Vector3Int _gridSize;

        private HashSet<Vector3Int> _takenGridPositions = new();
        private HashSet<Vector3Int> _wallCells;

        public StructureGridService(StructureGridCell structureGridCell, WallStructureGridCell wallStructureGridCell)
        {
            _cellPrefab = structureGridCell;
            _wallCellPrefab = wallStructureGridCell;
        }

        public void Initialize()
        {
            CreateGridCells();
        }

        public void ShowGrid() => ShowGridCells(GetCellsForBuilding());

        public void ShowGridForAtatchedBuilding()
        {
            var wallCells = GetCellsForAttatching();
            _wallCells = new HashSet<Vector3Int>(wallCells.Select(p => p.position));
            ShowWallGridCells(wallCells);
        }

        public void SetGridSize(Vector3Int size) => _gridSize = size;

        public void HideGrid()
        {
            foreach (var cell in _gridCells)
            {
                cell.SetActive(false);
                cell.ResetToDefaultState();
            }

            foreach (var cell in _wallGridCells)
            {
                cell.SetActive(false);
                cell.ResetToDefaultState();
            }
        }

        public void AddTakenGridCells(IEnumerable<Vector3Int> positions)
        {
            foreach (var position in positions)
            {
                AddTakenGridCell(position);
            }
        }

        public void AddTakenGridCell(Vector3Int position)
        {
            _takenGridPositions.Add(position);
        }

        public void SetCellType(Vector3Int position, GridCellType type) => CellTypes[position] = type;

        public void SetCellsType(Dictionary<Vector3Int, GridCellType> cellTypes) => CellTypes = cellTypes;

        public bool AreWallGridCellsFree(IEnumerable<Vector3Int> requiredCells)
        {
            foreach(var cell in requiredCells)
            {
                if (!_wallCells.Contains(cell))
                {
                    return false;
                }
            }

            return true;
        }

        public bool AreGridCellsTaken(IEnumerable<Vector3Int> positions)
        {
            foreach (var position in positions)
            {
                if (_takenGridPositions.Contains(position))
                {
                    return true;
                }
            }

            return false;
        }

        public GridCellType GetCellType(Vector3Int position)
        {
            if (!CellTypes.ContainsKey(position))
            {
                return GridCellType.None;
            }

            return CellTypes[position];
        }



        public void AddTakenGridCells(IEnumerable<Vector3Int> cells, GridCellType type)
        {
            foreach (var cell in cells)
            {
                _takenGridPositions.Add(cell);
                CellTypes[cell] = type;
            }
        }

        public bool AreGridCellsFree(IEnumerable<Vector3Int> cells, GridCellType type)
        {
            var down = Vector3Int.down;

            foreach (var position in cells)
            {
                if (_takenGridPositions.Contains(position))
                {
                    return false;
                }

                var downPosition = position + down;
                if (!_takenGridPositions.Contains(downPosition))
                {
                    return false;
                }

                if (!type.HasFlag(CellTypes[downPosition]))
                {
                    return false;
                }
            }

            return true;
        }

        public bool AreWallGridCellsFree(IEnumerable<Vector3Int> cells, GridCellType type)
        {
            throw new NotImplementedException();
        }

        private void CreateGridCells()
        {
            CreateCells(_cellPrefab, _gridCells, InitialGridCellsAmount);
            CreateCells(_wallCellPrefab, _wallGridCells, InitialWallGridCellsAmount);

            void CreateCells<T>(T prefab, List<T> collection, int limit) where T : BaseStructureGridCell
            {
                for (int i = 0; i < limit; i++)
                {
                    var gridCell = UnityEngine.Object.Instantiate(prefab);
                    gridCell.SetSelectedAction(cell => GridCellSelected?.Invoke(cell));
                    gridCell.SetActive(false);
                    collection.Add(gridCell);
                }
            }
        }

        private void ShowGridCells(List<Vector3Int> positions)
        {
            for (int i = 0; i < positions.Count; i++)
            {
                var cell = _gridCells[i];
                cell.SetPosition(positions[i]);
                cell.SetActive(true);
            }
        }

        private void ShowWallGridCells(List<(Vector3Int position, Vector3Int wallPosition)> wallPositions)
        {
            for (int i = 0; i < wallPositions.Count; i++)
            {
                var (position, wallPosition) = wallPositions[i];
                var cell = _wallGridCells[i];
                cell.SetPosition(position, wallPosition);
                cell.SetActive(true);
            }
        }

        private List<(Vector3Int position, Vector3Int wallPosition)> GetCellsForAttatching()
        {
            var result = new List<(Vector3Int position, Vector3Int wallPosition)>();

            foreach (var takenPosition in _takenGridPositions)
            {
                var takenPositionsNeighbours = takenPosition.GetNeighbours();
                foreach (var neighbour in takenPositionsNeighbours)
                {
                    if (_takenGridPositions.Contains(neighbour))
                    {
                        continue;
                    }

                    result.Add((neighbour, takenPosition));
                }
            }

            return result;
        }

        public List<Vector3Int> GetCellsForBuilding()
        {
            var result = new List<Vector3Int>();
            var up = Vector3Int.up;

            foreach (var takenPosition in _takenGridPositions)
            {
                var position = takenPosition + up;
                if(!_takenGridPositions.Contains(position) && IsInsideGrid(position))
                {
                    result.Add(position);
                }
            }

            return result;
        }

        private bool IsInsideGrid(Vector3Int position)
        {
            if(position.x < 0 || position.y < 0 || position.z < 0)
            {
                return false;
            }

            if(position.x >= _gridSize.x || position.y >= _gridSize.y || position.z >= _gridSize.z)
            {
                return false;
            }

            return true;
        }
    }
}