﻿using System;

namespace Assets.Scripts.Services.StructureGridServices
{
    [Flags]
    public enum GridCellType
    {
        None = 1,
        Stone = 2,
        Mushrooms = 4,
        Building = 8,
    }
}