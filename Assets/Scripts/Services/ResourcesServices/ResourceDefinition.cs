using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Services.ResourcesServices
{

    [CreateAssetMenu(menuName = "Data/" + nameof(ResourceDefinition))]
    public class ResourceDefinition : ScriptableObject
    {
        public ResourceType ResourceType;
        public string Name;
        public string Description;
        public Sprite Icon;
        public bool HasLimitedCapacity;
    }
}