﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Services.ResourcesServices
{
    [CreateAssetMenu(menuName = "Data/" + nameof(ResourceDefinitionProvider), fileName = nameof(ResourceDefinitionProvider))]
    public class ResourceDefinitionProvider : ScriptableObject
    {
        [SerializeField] private List<ResourceDefinition> _resourceDefinitions;

        public IReadOnlyList<ResourceDefinition> ResourceDefinitions => _resourceDefinitions;

        public ResourceDefinition GetResourceDefinition(ResourceType type) => _resourceDefinitions.Find(r => r.ResourceType == type);
    }
}