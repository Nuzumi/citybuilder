using System;
using System.Diagnostics;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Services.ResourcesServices
{
    public class ResourceUpdateTimer : ITickable
    {
        private const float BaseClockTickInterval = 1f;

        public event Action ResourceUpdate;

        private float _lastTimeTick;

        public void Tick()
        {

            if(_lastTimeTick + BaseClockTickInterval < Time.time)
            {
                _lastTimeTick = Time.time;
                ResourceUpdate?.Invoke();
            }
        }
    }
}