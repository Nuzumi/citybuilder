﻿using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.Services.ResourcesServices
{
    public class CityResource
    {
        public event Action ResourceUpdated;

        public ResourceType Type => _definition.ResourceType;
        public bool HasLimitedCapacity => _definition.HasLimitedCapacity;
        public string Name => _definition.Name;
        public Sprite Icon => _definition.Icon;

        public UpdatableProperty<int> Amount { get; }
        public UpdatableProperty<int> Capacity { get; }
        public UpdatableProperty<int> ResourceGeneration { get; }


        private ResourceDefinition _definition;

        public CityResource(ResourceDefinition resourceDefinition)
        {
            _definition = resourceDefinition;

            Amount = new UpdatableProperty<int>(true);
            Capacity = new UpdatableProperty<int>(HasLimitedCapacity);
            ResourceGeneration = new UpdatableProperty<int>(true);

            Amount.ValueUpdated += () => ResourceUpdated?.Invoke();
            Capacity.ValueUpdated += () => ResourceUpdated?.Invoke();
            ResourceGeneration.ValueUpdated += () => ResourceUpdated?.Invoke();
        }
    }
}