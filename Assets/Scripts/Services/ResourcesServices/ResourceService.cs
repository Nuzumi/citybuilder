using Services.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using Zenject;

namespace Assets.Scripts.Services.ResourcesServices
{

    public class ResourceService
    {
        public event Action AnyResourceUpdated;

        public IReadOnlyList<CityResource> CityResources => _resources;

        private readonly ResourceDefinitionProvider _resourceDefinitionProvider;
        private readonly List<CityResource> _resources;

        public ResourceService(ResourceDefinitionProvider resourceDefinitionProvider)
        {
            _resourceDefinitionProvider = resourceDefinitionProvider;

            _resources = _resourceDefinitionProvider.ResourceDefinitions.Select(CreateResource).ToList();
        }

        public CityResource GetResource(ResourceType type) => _resources.Find(resource => resource.Type == type);

        private CityResource CreateResource(ResourceDefinition definition)
        {
            var resource = new CityResource(definition);
            resource.ResourceUpdated += () => AnyResourceUpdated?.Invoke();
            return resource;
        }
    }
}