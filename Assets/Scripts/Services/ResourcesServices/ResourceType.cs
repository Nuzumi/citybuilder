﻿namespace Assets.Scripts.Services.ResourcesServices
{
    public enum ResourceType
    {
        None,
        People,
        Energy,
        Money,
        Water,
        Food,
        Stones
    }
}