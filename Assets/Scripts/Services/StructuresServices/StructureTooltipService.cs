using Assets.Scripts.Services.BuildingServices;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Services.StructuresServices
{
    public class StructureTooltipService
    {
        public event Action<StructureDescription> EnableTooltip;
        public event Action DisableTooltip;

        public void ShowTooltip(StructureDescription description)
        {
            EnableTooltip?.Invoke(description);
        }

        public void HideTooltip()
        {
            DisableTooltip?.Invoke();
        }
    }
}