using Assets.Scripts.BuildingElements;
using System.Collections.Generic;
using static Assets.Scripts.Services.BuildingServices.StructuresPrefabProvider;

namespace Assets.Scripts.Services.StructuresServices
{
    public class StructuresService
    {
        public List<(StructureBuildingElement buildingElement, StructureBuildingPrefabData prefabData)> StructureData { get; } = new();

        public void RegisterStructure(StructureBuildingElement buildingElement, StructureBuildingPrefabData prefabData)
        {
            StructureData.Add((buildingElement, prefabData));
        }
    }
}