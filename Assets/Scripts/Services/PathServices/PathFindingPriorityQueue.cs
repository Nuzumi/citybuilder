﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Services.PathServices
{
    public class PathFindingPriorityQueue
    {
        public int Count => _list.Count;

        private readonly List<PathFindingNode> _list = new();

        public void Enqueue(PathFindingNode node)
        {
            for(int i = 0; i < _list.Count; i++)
            {
                if (_list[i].Distance > node.Distance)
                {
                    _list.Insert(i, node);
                    return;
                }
            }

            _list.Add(node);
        }

        public PathFindingNode Dequeue()
        {
            var node = _list[0];
            _list.RemoveAt(0);
            return node;
        }

        public bool ContainsNode(Vector3Int position) => _list.FindIndex(node => node.position == position) != -1;

        public PathFindingNode GetNode(Vector3Int position) => _list.Find(node => node.position == position);

        public void RemoveNode(Vector3Int position)
        {
            var index = _list.FindIndex(node => node.position == position);
            if(index == -1)
            {
                return;
            }

            _list.RemoveAt(index);
        }

        public void Clear()
        {
            _list.Clear();
        }
    }
}