﻿using Assets.Scripts.Utils.Debug;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Services.PathServices
{
    public class PathGraphDictionary
    {
        private readonly Dictionary<Vector3Int, PathCell> _graph = new();

        public PathCell this[Vector3Int key]
        {
            get => _graph[key];
            set => Add(key, value);
        }

        public void Add(Vector3Int key, PathCell value)
        {
            _graph.Add(key, value);

            foreach (var neighbour in value.NeighboursConnectionPoints.Keys.ToList())
            {
                if (!_graph.ContainsKey(neighbour))
                {
                    continue;
                }

                var neighbourCell = _graph[neighbour];

                if (!neighbourCell.NeighboursConnectionPoints.ContainsKey(key))
                {
                    continue;
                }

                PathCell lowerPriority;
                PathCell higherPriority;
                if(value.NeighbourPriority >= neighbourCell.NeighbourPriority)
                {
                    lowerPriority = neighbourCell;
                    higherPriority = value;
                }
                else
                {
                    lowerPriority = value;
                    higherPriority = neighbourCell;
                }

                lowerPriority.RemovedNeighboursConnectionsPoints[higherPriority.Position] = lowerPriority.NeighboursConnectionPoints[higherPriority.Position]; 
                lowerPriority.NeighboursConnectionPoints[higherPriority.Position] = higherPriority.NeighboursConnectionPoints[lowerPriority.Position];
            }

            DrawGizmos();
        }

        public void Remove(Vector3Int key)
        {
            if (!_graph.ContainsKey(key))
            {
                return;
            }

            var removedCell = _graph[key];
            _graph.Remove(key);

            foreach (var neighbour in removedCell.NeighboursConnectionPoints.Keys)
            {
                if (!_graph.ContainsKey(neighbour))
                {
                    continue;
                }

                var neighbourCell = _graph[neighbour];

                var keys = neighbourCell.RemovedNeighboursConnectionsPoints.Keys.ToList();
                for (int i = 0; i < keys.Count; i++)
                {
                    var removedNeighbour = keys[i];
                    var removedNeighbourValue = neighbourCell.RemovedNeighboursConnectionsPoints[removedNeighbour];

                    neighbourCell.NeighboursConnectionPoints[removedNeighbour] = removedNeighbourValue;
                    neighbourCell.RemovedNeighboursConnectionsPoints.Remove(removedNeighbour);
                }
            }

            DrawGizmos();
        }

        public bool ContainsKey(Vector3Int key) => _graph.ContainsKey(key);

        private void DrawGizmos()
        {
            GizmosController.ClearGizmosToDraw();

            foreach(var node in _graph.Values)
            {
                GizmosController.AddGizmoToDraw(new DebugGizmo()
                {
                    color = Color.green,
                    radius = .15f,
                    position = node.Position
                });

                foreach (var connectionPoint in node.NeighboursConnectionPoints.Values)
                {
                    if (!connectionPoint.HasValue)
                    {
                        continue;
                    }

                    GizmosController.AddGizmoToDraw(new DebugGizmo()
                    {
                        color = Color.red,
                        radius = .1f,
                        position = connectionPoint.Value
                    });
                }
            }
        }
    }
}