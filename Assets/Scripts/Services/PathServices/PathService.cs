using Assets.Scripts.BuildingElements;
using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.SaveLoadServices;
using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.Services.StructuresServices;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Services.PathServices
{
    public class PathCell
    {
        public Vector3Int Position;
        public int NeighbourPriority;
        public Dictionary<Vector3Int, Vector3?> NeighboursConnectionPoints;
        public Dictionary<Vector3Int, Vector3?> RemovedNeighboursConnectionsPoints;
    }

    public class PathService : IInitializable, IDisposable
    {
        public bool IsInitialized { get; private set; }
        public event Action PathServiceInitialized;

        private readonly StructureGridService _structureGridService;
        private readonly StructuresBuildingService _structuresBuildingService;
        private readonly LoadService _loadService;
        private readonly StructuresService _structuresService;
        private readonly PathCacheService _pathCacheService;

        private readonly PathGraphDictionary _pathGraph = new();
        private readonly PathFinder _pathFinder;

        public PathService(
            StructureGridService structureGridService,
            StructuresBuildingService structuresBuildingService,
            LoadService loadService,
            StructuresService structuresService,
            PathCacheService pathCacheService)
        {
            _structureGridService = structureGridService;
            _structuresBuildingService = structuresBuildingService;
            _loadService = loadService;
            _structuresService = structuresService;
            _pathCacheService = pathCacheService;
            _pathFinder = new PathFinder(_pathGraph);
        }

        public bool DoesPathExist(Vector3Int start, Vector3Int finish)
        {
            if(_pathCacheService.ContainsPath(start, finish))
            {
                return true;
            }

            var path = _pathFinder.GetPath(start, finish);
            _pathCacheService.AddPath(path);

            return path.Any();
        }

        public List<Vector3> GetPath(Vector3Int start, Vector3Int finish)
        {
            var cellPath = _pathFinder.GetPath(start, finish);
            _pathCacheService.AddPath(cellPath);

            var path = new List<Vector3>();

            for (int i = 0; i < cellPath.Count - 1; i++)
            {
                var cell = _pathGraph[cellPath[i]];
                var nextCell = cellPath[i + 1];

                path.Add(cell.NeighboursConnectionPoints[nextCell].Value);
            }

            path.Add(finish);
            return path;
        }

        public void Initialize()
        {
            _structuresBuildingService.BuildFinished += OnBuildingFinished;
            _loadService.GameLoaded += OnGameLoaded;
        }

        public void Dispose()
        {
            _structuresBuildingService.BuildFinished -= OnBuildingFinished;
            _loadService.GameLoaded -= OnGameLoaded;
        }

        private void OnBuildingFinished()
        {
            var structure = _structuresBuildingService.LasBuildElement;

            foreach(var createdCell in structure.CreatedCells)
            {
                AddEmptyCellPathCells(createdCell);
            }

            foreach(var requiredCell in structure.RequiredCells.cells)
            {
                _pathGraph.Remove(requiredCell);
            }

            foreach (var takenCell in structure.TakenCells.cells)
            {
                _pathGraph.Remove(takenCell);
            }

            foreach (var cell in structure.GetStructurePathCell())
            {
                _pathGraph[cell.Position] = cell;
            }
        }

        private void OnGameLoaded()
        {
            AddEmptyCellsPathCells();
            RemoveTakenPathCells();
            AddStructurePathCells();

            IsInitialized = true;
            PathServiceInitialized?.Invoke();
        }

        private void AddEmptyCellsPathCells()
        {
            foreach (var position in _structureGridService.GetCellsForBuilding())
            {
                AddEmptyCellPathCells(position);    
            }
        }

        private void AddEmptyCellPathCells(Vector3Int position)
        {
            if (_pathGraph.ContainsKey(position))
            {
                return;
            }

            var cell = new PathCell()
            {
                Position = position,
                NeighbourPriority = 0,
                NeighboursConnectionPoints = new (),
                RemovedNeighboursConnectionsPoints = new()
            };

            foreach(var neighbourPosition in position.GetNeighbours())
            {
                cell.NeighboursConnectionPoints[neighbourPosition] = GetConnectionPoint(position, neighbourPosition);
            }

            _pathGraph.Add(position, cell);

            Vector3 GetConnectionPoint(Vector3Int from, Vector3Int to)
            {
                return (from + (Vector3)to) / 2f;
            }
        }

        private void RemoveTakenPathCells()
        {
            foreach(var takenPosition in _structureGridService.TakenGridPositions)
            {
                _pathGraph.Remove(takenPosition);
            }
        }

        private void AddStructurePathCells()
        {
            foreach (var structureBuildingElement in _structuresService.StructureData.Select(p => p.buildingElement))
            {
                foreach (var cell in structureBuildingElement.GetStructurePathCell())
                {
                    _pathGraph[cell.Position] = cell;
                }
            }
        }
    }
}