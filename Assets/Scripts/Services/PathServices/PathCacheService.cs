﻿using Assets.Scripts.Services.BuildingServices;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Services.PathServices
{
    public class PathCacheService : IInitializable
    {
        private const int MaxCachedpaths = 50;

        private readonly Dictionary<(Vector3Int start, Vector3Int finish), CachedPath> _pathsCache = new ();

        private readonly StructuresBuildingService _structuresBuildingService;

        public PathCacheService(StructuresBuildingService structuresBuildingService)
        {
            _structuresBuildingService = structuresBuildingService;
        }

        public void Initialize()
        {
            _structuresBuildingService.BuildFinished += OnBuildFinished;
        }

        public bool ContainsPath(Vector3Int start, Vector3Int finish)
        {
            return _pathsCache.ContainsKey((start, finish));
        }

        public List<Vector3Int> GetPath(Vector3Int start, Vector3Int finish)
        {
            SetPathAsUsed(start, finish);
            return _pathsCache[(start, finish)].path;
        }

        public void AddPath(List<Vector3Int> path)
        {
            var start = path[0];
            var finish = path[path.Count - 1];

            if(ContainsPath(start, finish))
            {
                return;
            }

            var cachedPath = new CachedPath()
            {
                path = path,
                pathPoints = new HashSet<Vector3Int>(path)
            };

            _pathsCache[(start, finish)] = cachedPath;
            SetPathAsUsed(start, finish);
            TryRemoveOldestPath();
        }

        private void OnBuildFinished()
        {
            var structure = _structuresBuildingService.LasBuildElement;
            var cells = new List<Vector3Int>();

            cells.AddRange(structure.RequiredCells.cells);
            cells.AddRange(structure.TakenCells.cells);

            var invalidPaths = new List<(Vector3Int, Vector3Int)>();

            foreach (var pathKey in _pathsCache.Keys)
            {
                foreach (var cell in cells)
                {
                    if (_pathsCache[pathKey].pathPoints.Contains(cell))
                    {
                        invalidPaths.Add(pathKey);
                        continue;
                    }
                }
            }

            foreach (var pathKey in invalidPaths)
            {
                _pathsCache.Remove(pathKey);
            }
        }

        private void SetPathAsUsed(Vector3Int start, Vector3Int finish)
        {
            _pathsCache[(start, finish)].lastUsedTime = Time.time;
        }

        private void TryRemoveOldestPath()
        {
            if(_pathsCache.Count <= MaxCachedpaths)
            {
                return;
            }

            CachedPath oldestPath = _pathsCache.Values.First();

            foreach (var pathKey in _pathsCache.Keys)
            {
                var path = _pathsCache[pathKey];
                if(path.lastUsedTime < oldestPath.lastUsedTime)
                {
                    oldestPath = path;
                }
            }

            _pathsCache.Remove((oldestPath.path.First(), oldestPath.path.Last()));
        }

        private class CachedPath
        {
            public float lastUsedTime;
            public HashSet<Vector3Int> pathPoints;
            public List<Vector3Int> path;
        }
    }
}