﻿using Assets.Scripts.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Services.PathServices
{
    public struct PathFindingNode
    {
        public float Distance => distanceFromStart + estimatedDistanceToTarget;

        public Vector3Int position;
        public int distanceFromStart;
        public int estimatedDistanceToTarget;
        public Vector3Int? predecesssor;
    }

    public class PathFinder
    {
        private readonly PathGraphDictionary _pathGraph;
        private readonly Dictionary<Vector3Int, PathFindingNode> _closedList;
        private readonly PathFindingPriorityQueue _queue;


        public PathFinder(PathGraphDictionary pathGraph)
        {
            _pathGraph = pathGraph;
            _closedList = new Dictionary<Vector3Int, PathFindingNode>();
            _queue = new PathFindingPriorityQueue();
        }

        public List<Vector3Int> GetPath(Vector3Int start, Vector3Int finish)
        {
            _closedList.Clear();
            _queue.Clear();

            var startNode = new PathFindingNode()
            {
                distanceFromStart = 0,
                estimatedDistanceToTarget = start.GetManchattanDistance(finish),
                position = start,
                predecesssor = null,
            };

            _queue.Enqueue(startNode);

            while(_queue.Count > 0)
            {
                var currentNode = _queue.Dequeue();
                _closedList[currentNode.position] = currentNode;
                var neighbours = GetNeighbours(currentNode, finish);

                foreach (var neighbour in neighbours)
                {
                    if(neighbour.position == finish)
                    {
                        return ConstructPath(neighbour);
                    }

                    if(_queue.ContainsNode(neighbour.position) 
                        && _queue.GetNode(neighbour.position).Distance > neighbour.Distance)
                    {
                        _queue.RemoveNode(neighbour.position);
                        _queue.Enqueue(neighbour);
                        continue;
                    }

                    if(_closedList.ContainsKey(neighbour.position)
                        && _closedList[neighbour.position].Distance < neighbour.Distance)
                    {
                        continue;
                    }

                    _queue.Enqueue(neighbour);
                }
            }

            return new List<Vector3Int>();
        }

        private List<Vector3Int> ConstructPath(PathFindingNode target)
        {
            var result = new List<Vector3Int>();

            var currentNode = target;

            while (currentNode.predecesssor.HasValue)
            {
                result.Add(currentNode.position);
                currentNode = _closedList[currentNode.predecesssor.Value];
            }

            result.Add(currentNode.position);

            result.Reverse();

            return result;
        }

        private List<PathFindingNode> GetNeighbours(PathFindingNode node, Vector3Int target)
        {
            var result = new List<PathFindingNode>();
            var cell = _pathGraph[node.position];
            foreach (var neighbour in cell.NeighboursConnectionPoints.Keys)
            {
                if (!cell.NeighboursConnectionPoints[neighbour].HasValue)
                {
                    continue;
                }

                if (!_pathGraph.ContainsKey(neighbour))
                {
                    continue;
                }

                result.Add(new PathFindingNode()
                {
                    position = neighbour,
                    distanceFromStart = node.distanceFromStart + 1,
                    estimatedDistanceToTarget = neighbour.GetManchattanDistance(target),
                    predecesssor = node.position
                });
            }

            return result;
        }
    }
}