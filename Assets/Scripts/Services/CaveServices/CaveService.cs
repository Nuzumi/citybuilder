using Assets.Scripts.MapGeneration;
using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.Utils;
using System;
using UnityEngine;

namespace Assets.Scripts.Services.CaveServices
{
    public class CaveService
    {
        private readonly CaveGeneratorPerlin _caveGeneratorPerlin;
        private readonly MeshFilter _caveMesh;
        private readonly CaveMeshCreator _caveMeshCreator;
        private readonly StructureGridService _structureGridService;
        private readonly CaveCellTypesGenerator _caveCellTypesGenerator;

        private bool[,,] _caveData;

        public CaveService(
            CaveGeneratorPerlin caveGeneratorPerlin,
            MeshFilter caveMesh,
            CaveMeshCreator caveMeshCreator,
            StructureGridService structureGridService,
            CaveCellTypesGenerator caveCellTypesGenerator)
        {
            _caveGeneratorPerlin = caveGeneratorPerlin;
            _caveMesh = caveMesh;
            _caveMeshCreator = caveMeshCreator;
            _structureGridService = structureGridService;
            _caveCellTypesGenerator = caveCellTypesGenerator;
        }

        public bool[,,] GetCaveData() => _caveData;

        public void SetCaveData(bool[,,] caveData)
        {
            _caveData = caveData;

            TryCreateNewCave();

            _structureGridService.SetGridSize(new Vector3Int(_caveData.GetLength(0), _caveData.GetLength(1), _caveData.GetLength(2)));
            _caveMesh.mesh = _caveMeshCreator.CreateMesh(_caveData);
        }

        private void TryCreateNewCave()
        {
            if (_caveData != null)
            {
                return;
            }

            _caveData = _caveGeneratorPerlin.GenerateCave();
            AddCaveGridCells();
            SetCavesTilesType();
        }

        private void AddCaveGridCells() => IterateAllPositions(TryAddCaveGridCell);

        private void TryAddCaveGridCell(int x, int y, int z)
        {
            var position = new Vector3Int(x, y, z);

            if (!_caveData[x, y, z])
            {
                return;
            }
            
            _structureGridService.AddTakenGridCell(position);
        }

        private void SetCavesTilesType()
        {
            var cellTypes = _caveCellTypesGenerator.GetCellTypes(_caveData);
            cellTypes.ForEach(cellType => _structureGridService.SetCellType(cellType.position, cellType.type));
        }

        private void IterateAllPositions(Action<int, int, int> action) => _caveData.IterateAllPositions(action);

        private bool IsOnGrid(Vector3Int position) => _caveData.IsInArray(position);
    }
}