﻿using System;
using UnityEngine;

namespace Assets.Scripts.Ui
{
    public abstract class UiElement : MonoBehaviour
    {
        public event Action Destroyed;

        private void OnDestroy() => Destroyed?.Invoke();
    }
}