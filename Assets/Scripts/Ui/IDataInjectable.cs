﻿namespace Assets.Scripts.Ui
{
    public interface IDataInjectable<T>
    {
        void InjectData(T data);
    }
}