﻿using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.ResourcesServices;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Ui.StructureTooltip
{
    public class NoCostGeneratorTooltipController : StructureTooltipBaseController
    {
        [SerializeField] private TextMeshProUGUI _generationProgress;
        [SerializeField] private TooltipResourceDisplay _resourceDisplay;
        [SerializeField] private Transform _resourceGenerationParent;

        public override bool CanDisplayDescription(StructureDescription description)
        {
            return description.StructureTooltipType == StructureTooltipType.NoCostGenerator;
        }

        public override void Initialize(StructureDescription description, ResourceDefinitionProvider resourceDefinitionProvider)
        {
            foreach (var resource in description.GeneratedResources)
            {
                var icon = resourceDefinitionProvider.GetResourceDefinition(resource.type).Icon;

                var resourceDisplay = Instantiate(_resourceDisplay, _resourceGenerationParent);
                resourceDisplay.Initialize(icon, resource.amount);
                _resources.Add(resourceDisplay.gameObject);
            }

            _generationProgress.text = $"{description.CurrentGenerationCycle} / {description.CyclesToCreateResource}";
        }
    }
}