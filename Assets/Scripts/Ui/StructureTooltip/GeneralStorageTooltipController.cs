﻿using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.ResourcesServices;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Ui.StructureTooltip
{
    public class GeneralStorageTooltipController : StructureTooltipBaseController
    {
        [SerializeField] private TextMeshProUGUI _totalCapacity;
        [SerializeField] private TextMeshProUGUI _currentCapacity;
        [SerializeField] private TooltipResourceDisplay _resourceDisplay;
        [SerializeField] private Transform _resourceParent;

        public override void Initialize(StructureDescription description, ResourceDefinitionProvider resourceDefinitionProvider)
        {
            var currentCapacity = 0;

            foreach (var resource in description.StoredResources)
            {
                currentCapacity += resource.amount;

                var icon = resourceDefinitionProvider.GetResourceDefinition(resource.type).Icon;
                var resourceDisplay = Instantiate(_resourceDisplay, _resourceParent);
                resourceDisplay.Initialize(icon, resource.amount);
                _resources.Add(resourceDisplay.gameObject);
            }


            _totalCapacity.text = description.GeneralStorageCapacity.ToString();
            _currentCapacity.text = currentCapacity.ToString();
        }

        public override bool CanDisplayDescription(StructureDescription description)
        {
            return description.StructureTooltipType == StructureTooltipType.GeneralStructureStorage;
        }
    }
}