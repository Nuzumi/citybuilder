using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.Services.StructuresServices;
using Assets.Scripts.Utils.Attributes;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Ui.StructureTooltip
{
    [Prefab(UiELementLayer.HUD, true)]
    public class StructureTooltipDisplay : UiElement
    {
        [SerializeField] private TextMeshProUGUI _structureName;
        [SerializeField] private List<StructureTooltipBaseController> _tooltipControllers;

        private StructureTooltipService _tooltipService;
        private StructuresPrefabProvider _structurePrefabProvider;
        private ResourceDefinitionProvider _resourceDefinitionProvider;

        [Inject]
        public void Initialize(StructureTooltipService tooltipService, StructuresPrefabProvider structuresPrefabProvider, ResourceDefinitionProvider resourceDefinitionProvider)
        {
            _tooltipService = tooltipService;
            _structurePrefabProvider = structuresPrefabProvider;
            _resourceDefinitionProvider = resourceDefinitionProvider;
        }

        private void Start()
        {
            gameObject.SetActive(false);
            _tooltipService.EnableTooltip += OnEnableTooltip;
            _tooltipService.DisableTooltip += OnDisableTooltip;
        }

        private void OnDestroy()
        {
            _tooltipService.EnableTooltip -= OnEnableTooltip;
            _tooltipService.DisableTooltip -= OnDisableTooltip;
        }

        private void OnEnableTooltip(StructureDescription description)
        {
            ResetTooltip();
            var data = _structurePrefabProvider.GetStructureData(description.StructureId);

            if(data == null)
            {
                return;
            }

            _structureName.text = data.name;
            var controller = GetController(description);
            controller.Initialize(description, _resourceDefinitionProvider);
            controller.SetActive(true);

            gameObject.SetActive(true);
        }

        private void OnDisableTooltip()
        {
            gameObject.SetActive(false);
        }

        public StructureTooltipBaseController GetController(StructureDescription description)
        {
            return _tooltipControllers.Find(tc => tc.CanDisplayDescription(description));
        }

        private void ResetTooltip()
        {
            _tooltipControllers.ForEach(tooltip => tooltip.SetActive(false));
        }
    }
}