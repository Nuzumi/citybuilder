﻿using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.ResourcesServices;
using UnityEngine;

namespace Assets.Scripts.Ui.StructureTooltip
{

    public class UnderConstructionTooltipController : StructureTooltipBaseController
    {
        [SerializeField] private Transform _resourceParent;
        [SerializeField] private TooltipResourceWitchCapacityDisplay _resourceDisplay;

        public override bool CanDisplayDescription(StructureDescription description)
        {
            return description.StructureTooltipType == StructureTooltipType.UnderContruction;
        }

        public override void Initialize(StructureDescription description, ResourceDefinitionProvider resourceDefinitionProvider)
        {
            foreach (var item in description.BuildingCost)
            {
                var storedResource = description.StoredResources.Find(r => r.type == item.type);
                var storedAmount = storedResource?.amount ?? 0;

                var icon = resourceDefinitionProvider.GetResourceDefinition(item.type).Icon;
                var resourceDisplay = Instantiate(_resourceDisplay, _resourceParent);
                resourceDisplay.Initialize(icon, storedAmount, item.amount);
                _resources.Add(resourceDisplay.gameObject);
            }
        }
    }
}