﻿using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.ResourcesServices;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Ui.StructureTooltip
{
    public enum StructureTooltipType
    {
        None,
        UnderContruction,
        StructureStorage,
        GeneralStructureStorage,
        Generator,
        NoCostGenerator
    }

    public abstract class StructureTooltipBaseController : MonoBehaviour
    {
        protected List<GameObject> _resources = new();

        public void SetActive(bool active) => gameObject.SetActive(active);

        public abstract void Initialize(StructureDescription description, ResourceDefinitionProvider resourceDefinitionProvider);

        public abstract bool CanDisplayDescription(StructureDescription description);

        private void OnDisable()
        {
            Clear();
        }

        private void Clear()
        {
            foreach (var resource in _resources)
            {
                Destroy(resource);
            }

            _resources.Clear();
        }
    }
}