﻿using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.ResourcesServices;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Ui.StructureTooltip
{
    public class ResourceStorageTooltipController : StructureTooltipBaseController
    {
        [SerializeField] private TooltipResourceWitchCapacityDisplay _resourceDisplay;
        [SerializeField] private Transform _resourceParent;

        public override bool CanDisplayDescription(StructureDescription description)
        {
            return description.StructureTooltipType == StructureTooltipType.StructureStorage;
        }

        public override void Initialize(StructureDescription description, ResourceDefinitionProvider resourceDefinitionProvider)
        {
            foreach (var resource in description.MaxStoredResources)
            {
                var storedResource = description.StoredResources.Find(r => r.type == resource.type);
                var storedAmount = storedResource?.amount ?? 0;

                var icon = resourceDefinitionProvider.GetResourceDefinition(resource.type).Icon;
                var resourceDisplay = Instantiate(_resourceDisplay, _resourceParent);
                resourceDisplay.Initialize(icon, storedAmount, resource.amount);
                _resources.Add(resourceDisplay.gameObject);
            }
        }
    }
}