﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui.StructureTooltip
{
    public class TooltipResourceWitchCapacityDisplay : MonoBehaviour
    {
        private const string Separator = " / ";

        [SerializeField] private Image _icon;
        [SerializeField] private TextMeshProUGUI _amount;

        public void Initialize(Sprite icon, int amount, int max)
        {
            _icon.sprite = icon;
            _amount.text = $"{amount}{Separator}{max}";
        }
    }
}