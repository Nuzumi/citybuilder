﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui.StructureTooltip
{
    public class TooltipResourceDisplay : MonoBehaviour
    {
        [SerializeField] private Image _icon;
        [SerializeField] private TextMeshProUGUI _amount;

        public void Initialize(Sprite icon, int amount)
        {
            _icon.sprite = icon;
            _amount.text = amount.ToString();
        }
    }
}