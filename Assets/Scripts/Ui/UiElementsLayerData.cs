﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Ui
{
    public class UiElementsLayerData : MonoBehaviour
    {
        [SerializeField] private List<TransformLayerData> _layerData;

        public Transform GetLayerTransform(UiELementLayer layer) => _layerData.Find(d => d.PopupLayer == layer).Transform;

        [Serializable]
        public class TransformLayerData
        {
            public UiELementLayer PopupLayer;
            public Transform Transform;
        }
    }
}