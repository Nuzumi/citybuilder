﻿using System;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Ui
{
    public class PopupsFactory
    {
        private readonly UiElementsPrefabData _prefabData;
        private readonly DiContainer _container;

        public PopupsFactory(UiElementsPrefabData prefabData, DiContainer container)
        {
            _prefabData = prefabData;
            _container = container;
        }

        public GameObject CreatePopup(Type type, Transform parent)
        {
            var prefab = _prefabData.GetPrefab(type);
            var popup = UnityEngine.Object.Instantiate(prefab, parent).GetComponent(type);
            _container.Inject(popup);
            return popup.gameObject;
        }
    }
}