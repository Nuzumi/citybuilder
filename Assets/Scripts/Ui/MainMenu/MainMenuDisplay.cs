using Assets.Scripts.Services.UI.Views;
using Assets.Scripts.Utils.Attributes;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Assets.Scripts.Ui.MainMenu
{
    [Prefab(UiELementLayer.Popup, false)]
    public class MainMenuDisplay : UiElement
    {
        [SerializeField] private Button _continueButton;
        [SerializeField] private Button _newGameButton;

        private MainMenuService _mainMenuService;

        [Inject]
        public void Initialize(MainMenuService mainMenuService)
        {
            _mainMenuService = mainMenuService;
        }

        private void Start()
        {
            _continueButton.onClick.AddListener(() => _mainMenuService.LoadStationData());
            _newGameButton.onClick.AddListener(() => _mainMenuService.LoadInitialData());
        }

        private void OnDestroy()
        {
            _continueButton.onClick.RemoveAllListeners();
            _newGameButton.onClick.RemoveAllListeners();
        }
    }
}