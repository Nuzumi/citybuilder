﻿using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.Utils.Attributes;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Ui.StructuresDisplay
{

    [Prefab(UiELementLayer.HUD, true)]
    public class ResourcesDisplay : UiElement
    {
        [SerializeField] private Transform _elementsParent;
        [SerializeField] private ResourceDisplayElement _displayElement;

        private ResourceService _resourceService;

        [Inject]
        public void Initialize(ResourceService resourceService)
        {
            _resourceService = resourceService;
        }

        private void Start()
        {
            foreach (var resource in _resourceService.CityResources)
            {
                var resourceElement = Instantiate(_displayElement, _elementsParent);
                resourceElement.Initialize(resource);
            }
        }
    }
}