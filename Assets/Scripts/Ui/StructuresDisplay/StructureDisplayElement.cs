﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui.StructuresDisplay
{
    public class StructureDisplayElement : MonoBehaviour
    {
        [SerializeField] private Button _selectedButton;
        [SerializeField] private TextMeshProUGUI _name;

        private Action _buildingChosen;

        private void Start()
        {
            _selectedButton.onClick.AddListener(_buildingChosen.Invoke);
        }

        private void OnDestroy()
        {
            _selectedButton.onClick.RemoveAllListeners();
        }

        public void Initialize(Action buildingChosen, string name)
        {
            _buildingChosen = buildingChosen;
            _name.text = name;
        }
    }
}