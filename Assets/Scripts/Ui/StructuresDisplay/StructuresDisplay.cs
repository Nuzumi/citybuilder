using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Utils.Attributes;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Assets.Scripts.Ui.StructuresDisplay
{

    [Prefab(UiELementLayer.HUD, true)]
    public class StructuresDisplay : UiElement
    {
        [SerializeField] private Transform _elementsParent;
        [SerializeField] private StructureDisplayElement _displayElement;

        private StructuresPrefabProvider _prefabProvider;
        private StructuresBuildingService _buildingService;

        [Inject]
        public void Initialize(
            StructuresPrefabProvider prefabProvider,
            StructuresBuildingService buildingService)
        {
            _prefabProvider = prefabProvider;
            _buildingService = buildingService;
        }

        private void Start()
        {
            foreach (var prefab in _prefabProvider.PrefabsDataToDisplay)
            {
                var displayElement = Instantiate(_displayElement, _elementsParent);
                displayElement.Initialize(() => BuildingChosen(prefab), prefab.name);
            }
        }

        private void BuildingChosen(StructuresPrefabProvider.StructureBuildingPrefabData prefabData)
        {
            if (_buildingService.IsInBuildingMode)
            {
                return;
            }

            _buildingService.StartBuildingMode(prefabData);
        }
    }
}