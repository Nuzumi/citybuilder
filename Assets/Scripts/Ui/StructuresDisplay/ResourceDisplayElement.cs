﻿using Assets.Scripts.Services.ResourcesServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Ui.StructuresDisplay
{
    public class ResourceDisplayElement : MonoBehaviour
    {
        [SerializeField] private Image _iconImage;
        [SerializeField] private TextMeshProUGUI _amount;

        private CityResource _resource;

        public void Initialize(CityResource resource)
        {
            _resource = resource;
            _resource.Amount.ValueUpdated += SetAmountText;

            _iconImage.sprite = _resource.Icon;
            SetAmountText();
        }

        private void OnDestroy()
        {
            _resource.Amount.ValueUpdated -= SetAmountText;
        }

        private void SetAmountText()
        {
            _amount.text = _resource.Amount.Value.ToString();
        }
    }
}