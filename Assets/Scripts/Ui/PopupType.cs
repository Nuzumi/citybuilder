namespace Assets.Scripts.Ui
{
    public enum UiELementLayer
    {
        None,
        Popup,
        HUD
    }
}