using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Ui
{

    [CreateAssetMenu(menuName = "Data/" + nameof(UiElementsPrefabData), fileName = nameof(UiElementsPrefabData))]
    public class UiElementsPrefabData : ScriptableObject
    {
        private const string PopupsPrefabsPath = "Assets/UI/";

        [SerializeField] private List<GameObject> _popups;

        private Dictionary<Type, GameObject> _popupsDict;

        public GameObject GetPrefab(Type type) => _popupsDict[type];

        private void OnEnable()
        {
            if (_popupsDict is null && _popups is not null)
            {
                CreateDictionary();
            }
        }

        private void CreateDictionary()
        {
            _popupsDict = new Dictionary<Type, GameObject>();
            foreach (var popup in _popups)
            {
                var script = popup.GetComponent<UiElement>();
                _popupsDict.Add(script.GetType(), popup);
            }
        }

#if UNITY_EDITOR
        [ContextMenu("Scan for popups")]
        public void ScanProjectForPopups()
        {
            _popups = new List<GameObject>();
            var assetsPaths = Directory.GetFiles(PopupsPrefabsPath).ToList();
            var directories = Directory.GetDirectories(PopupsPrefabsPath, "*", SearchOption.AllDirectories);
            assetsPaths.AddRange(directories.SelectMany(d => Directory.GetFiles(d)));
            assetsPaths = assetsPaths.Distinct().ToList();
            assetsPaths = assetsPaths.Where(path => Path.GetExtension(path) == ".prefab").ToList();
            foreach (var assetPath in assetsPaths)
            {
                var asset = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                var script = asset.GetComponent<UiElement>();
                if (script != null)
                {
                    _popups.Add(asset);
                }
            }

            AssetDatabase.SaveAssets();
            CreateDictionary();
        }

        [ContextMenu("List popups")]
        public void ListPopups()
        {
            foreach (var data in _popupsDict)
            {
                Debug.Log($"{data.Key} {data.Value.name}");
            }
        }
#endif

    }
}