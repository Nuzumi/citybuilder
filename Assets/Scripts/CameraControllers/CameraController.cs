using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.CameraControllers
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _mouseSense;

        private InputActions.CameraControllActions _cameraActions;

        private Vector3 _moveDirection;
        private bool _rotationActive;

        private void Awake()
        {
            _cameraActions = new InputActions().CameraControll;
            _cameraActions.Enable();

            SubscribeToActions();
        }

        private void SubscribeToActions()
        {
            _cameraActions.Move.performed += SetMovePerformed;
            _cameraActions.Move.canceled += SetMoveCanceled;

            _cameraActions.MoveUpDown.performed += SetMoveUpDownPerformed;
            _cameraActions.MoveUpDown.canceled += SetMoveUpDownConceled;

            _cameraActions.RotationControll.performed += _ => _rotationActive = true;
            _cameraActions.RotationControll.canceled += _ => _rotationActive = false;
        }

        private void SetMovePerformed(InputAction.CallbackContext context)
        {
            var value = context.ReadValue<Vector2>();
            _moveDirection = new Vector3(value.x, _moveDirection.y, value.y);
        }

        private void SetMoveCanceled(InputAction.CallbackContext _)
        {
            _moveDirection = new Vector3(0, _moveDirection.y, 0);
        }

        private void SetMoveUpDownPerformed(InputAction.CallbackContext context)
        {
            var value = context.ReadValue<float>();
            _moveDirection = new Vector3(_moveDirection.x, value, _moveDirection.z);
        }

        private void SetMoveUpDownConceled(InputAction.CallbackContext _)
        {
            _moveDirection = new Vector3(_moveDirection.x, 0, _moveDirection.z);
        }

        private void Update()
        {
            var deltaTimeSpeed = (Time.unscaledDeltaTime * _speed);
            transform.position += transform.forward *  (_moveDirection.z * deltaTimeSpeed);
            transform.position += transform.right *  (_moveDirection.x * deltaTimeSpeed);
            transform.position += transform.up *  (_moveDirection.y * deltaTimeSpeed);

            if (_rotationActive)
            {
                var mouseDelta = _cameraActions.MousePosition.ReadValue<Vector2>();

                transform.rotation *= Quaternion.AngleAxis(
                -mouseDelta.y * _mouseSense,
                Vector3.right);

                transform.rotation = Quaternion.Euler(
                    transform.eulerAngles.x,
                    transform.eulerAngles.y + mouseDelta.x * _mouseSense,
                    transform.eulerAngles.z
                );
            }
        }
    }
}