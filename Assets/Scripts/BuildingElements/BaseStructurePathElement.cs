﻿using Assets.Scripts.Services.PathServices;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{
    public abstract class BaseStructurePathElement : MonoBehaviour
    {
        public abstract List<PathCell> GetPathCells();
    }
}