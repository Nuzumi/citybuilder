﻿using Assets.Scripts.Services.ResourcesServices;
using System.Collections.Generic;

namespace Assets.Scripts.BuildingElements
{
    public interface IStructureStorage
    {
        IReadOnlyDictionary<ResourceType, int> MaxStorage { get; }
        List<ResourceData> GetAllResources();
        bool CanStoreResource(ResourceData resource);
        void StoreResource(ResourceData resource);
        bool CanRemoveResource(ResourceData resource);
        void RemoveResource(ResourceData resource);
    }
}