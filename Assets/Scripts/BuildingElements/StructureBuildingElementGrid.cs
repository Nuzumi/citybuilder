﻿using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Assets.Scripts.BuildingElements.StructureGridPartData;

namespace Assets.Scripts.BuildingElements
{
    [Serializable]
    public class StructureGridPartData
    {
        public GridCellType cellType;
        public List<StructureGridPart> cells;

        [Serializable]
        public class StructureGridPart
        {
            public Vector3Int partBegining;
            public Vector3Int partEnd;
        }
    }

    [Serializable]
    public class StructureBuildingElementGrid
    {
        public bool CanBeAtatchedToTakenCell => _canBeAtatchedToTakenCell;
        public (IEnumerable<Vector3Int> cells, GridCellType type) GetRequiredCellsPosition(Vector3Int centerPosition, Quaternion rotation) 
            => (GetCellsFromGridParts(_requiredCells.cells, centerPosition, rotation), _requiredCells.cellType);
        public IEnumerable<Vector3Int> GetCreatedCellsPositions(Vector3Int centerPosition, Quaternion rotation) => GetCellsFromGridParts(_createdCells.cells, centerPosition, rotation);
        public (IEnumerable<Vector3Int> cells, GridCellType type) GetTakenCellsPositions(Vector3Int centerPosition, Quaternion rotation) 
            => (GetCellsFromGridParts(_takenCells.cells, centerPosition, rotation), _takenCells.cellType);


        [SerializeField] private bool _canBeAtatchedToTakenCell;
        [SerializeField] private StructureGridPartData _requiredCells;
        [SerializeField] private StructureGridPartData _createdCells;
        [SerializeField] private StructureGridPartData _takenCells;

        private IEnumerable<Vector3Int> GetCellsFromGridParts(List<StructureGridPart> gridParts, Vector3Int centerPosition, Quaternion rotation)
        {
            var positions = gridParts
                .SelectMany(cells => GetCellsPositions(cells))
                .Distinct();

            if (!positions.Any())
            {
                return Enumerable.Empty<Vector3Int>();
            }

            var pivotPosition = positions.First();

            return positions.Select(position => position.RotateVector(rotation, pivotPosition)).Select(position => position + centerPosition);
        }

        private List<Vector3Int> GetCellsPositions(StructureGridPart gridPart)
        {
            var result = new List<Vector3Int>();

            for (int x = gridPart.partBegining.x; x <= gridPart.partEnd.x; x++)
            {
                for (int z = gridPart.partBegining.z; z <= gridPart.partEnd.z; z++)
                {
                    for(int y = gridPart.partBegining.y; y <= gridPart.partEnd.y; y++)
                    {
                        result.Add(new Vector3Int(x, y, z));
                    }
                }
            }

            return result;
        }
    }
}