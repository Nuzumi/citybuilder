﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{
    [CreateAssetMenu(menuName = "Data/" + nameof(StructureStorageData))]
    public class StructureStorageData : ScriptableObject
    {
        public bool isPermamentStorage;
        public List<ResourceData> capacityCreated;
    }
}