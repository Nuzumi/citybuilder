﻿using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.Services.BuildingServices;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;
using Assets.Scripts.Services.SaveLoadServices;
using Assets.Scripts.Services.UnitMovementServices;

namespace Assets.Scripts.BuildingElements
{
    public class ResourceGenerator : StructureElement, IStructureStateConsumer
    {
        public bool HasReservedResources { get; private set; }

        [SerializeField] private ResourceGeneratorData _resourceGeneratorData;
        [SerializeField] private StructureStorage _structureStorageIn;
        [SerializeField] private StructureStorage _structureStorageOut;

        private ResourceUpdateTimer _resourceUpdateTimer;
        private ResourceService _resourceService;
        private StructureState _state;

        private bool _isGenerating;
        private int _generationCycle;

        [Inject]
        public void Initialize(ResourceUpdateTimer resourceUpdateTimer, ResourceService resourceService)
        {
            _resourceUpdateTimer = resourceUpdateTimer;
            _resourceService = resourceService;
        }

        public override void UpdateStructureSaveState(StructureSaveState state)
        {
            state.isGenerating = _isGenerating;
            state.generationCycle = _generationCycle;
        }

        public override void LoadStructureSaveState(StructureSaveState state)
        {
            _isGenerating = state.isGenerating;
            _generationCycle = state.generationCycle;
        }

        private void OnDestroy()
        {
            if (_resourceUpdateTimer == null)
            {
                return;
            }

            _resourceUpdateTimer.ResourceUpdate -= OnResourceUpdate;
        }

        private void OnResourceUpdate()
        {
            if (_isGenerating)
            {
                if(_generationCycle == _resourceGeneratorData.timerTicksToComplete)
                {
                    _isGenerating = false;
                    _generationCycle = 0;
                    AddGeneratedResourcesToStorage();
                }
                else
                {
                    _generationCycle++;
                }
            }

            if (CanStartResourceGeneration())
            {
                StartResourceGeneration();
            }
        }

        private bool CanStartResourceGeneration()
        {
            if (_isGenerating)
            {
                return false;
            }

            foreach (var resourceCost in _resourceGeneratorData.generationCost)
            {
                if (!_structureStorageIn.CanRemoveResource(resourceCost))
                {
                    return false;
                }
            }

            foreach (var resourceGeneration in _resourceGeneratorData.generatedResources)
            {
                if (!_structureStorageOut.CanStoreResource(resourceGeneration))
                {
                    return false;
                }
            }

            return true;
        }

        private void StartResourceGeneration()
        {
            _isGenerating = true;
            foreach (var resourceCost in _resourceGeneratorData.generationCost)
            {
                _resourceService.GetResource(resourceCost.type).Amount.Value -= resourceCost.amount;
                _structureStorageIn.RemoveResource(resourceCost);
            }
        }

        private void AddGeneratedResourcesToStorage()
        {
            foreach (var generatedResource in _resourceGeneratorData.generatedResources)
            {
                var resource = _resourceService.GetResource(generatedResource.type);
                resource.Amount.Value += generatedResource.amount;
                _structureStorageOut.StoreResource(generatedResource);
            }
        }

        private void TryRegisterIntrestPoints()
        {
            if(_structureStorageIn != null)
            {
                _structureStorageIn.RegisterAsIntrestPoint(UnitIntrestPointType.Consumer);
            }

            _structureStorageOut.RegisterAsIntrestPoint(UnitIntrestPointType.Generator);
        }

        public void SetStructureState(StructureState structureState)
        {
            _state = structureState;
            TryBegineActions();
            _state.StateChanged += TryBegineActions;
        }

        public void TryBegineActions()
        {
            if(_state.Type != StructureStateType.Completed)
            {
                return;
            }

            _resourceUpdateTimer.ResourceUpdate += OnResourceUpdate;
            TryRegisterIntrestPoints();
        }

        public override void UpdateDescription(StructureDescription description)
        {
            if(_state.Type != StructureStateType.Completed)
            {
                return;
            }


            description.CurrentGeneratingResource.Clear();
            description.CurrentGeneratingResource.AddRange(_resourceGeneratorData.generatedResources.Select(r => new ResourceData(r)));
            description.CurrentGenerationCycle = _generationCycle;
            description.CyclesToCreateResource = _resourceGeneratorData.timerTicksToComplete;

            description.StructureTooltipType = Ui.StructureTooltip.StructureTooltipType.NoCostGenerator;

            if (_structureStorageIn != null)
            {
                description.StoredResources.AddRange(_structureStorageIn.GetAllResources());
                description.StructureTooltipType = Ui.StructureTooltip.StructureTooltipType.Generator;
            }
            description.GeneratedResources.AddRange(_structureStorageOut.GetAllResources());
        }
    }
}