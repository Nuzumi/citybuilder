﻿using Assets.Scripts.Services.ResourcesServices;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{
    [CreateAssetMenu(menuName = "Data/" + nameof(PermamentStorageData))]
    public class PermamentStorageData : ScriptableObject
    {
        public int capacity;
        public List<ResourceType> possibleResources;
    }
}