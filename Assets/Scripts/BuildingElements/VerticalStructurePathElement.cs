﻿using Assets.Scripts.Services.PathServices;
using Assets.Scripts.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{
    public class VerticalStructurePathElement : BaseStructurePathElement
    {
        [SerializeField] private Vector3Int _bottomConnectionPosition;
        [SerializeField] private Vector3Int _topConnectionPosition;
        [SerializeField] private List<Vector3Int> _removePositions;

        private Vector3Int Position => transform.position.ToIntVector();
        private Quaternion Rotation => transform.localRotation;

        public override List<PathCell> GetPathCells()
        {
            var cell = new PathCell()
            {
                Position = Position,
                NeighbourPriority = 1,
                NeighboursConnectionPoints = new(),
                RemovedNeighboursConnectionsPoints = new()
            };

            cell.NeighboursConnectionPoints[Position + _bottomConnectionPosition.RotateVector(Rotation, Vector3Int.zero)] = GetBottomConnectionPoint();
            cell.NeighboursConnectionPoints[Position + _topConnectionPosition.RotateVector(Rotation, Vector3Int.zero)] = GetTopConnectionPoint();

            foreach (var position in _removePositions)
            {
                cell.NeighboursConnectionPoints[Position + position.RotateVector(Rotation, Vector3Int.zero)] = null;
            }

            return new List<PathCell>() { cell };
        }

        private Vector3 GetBottomConnectionPoint()
        {
            var bottomNeighbourPosition = Position + _bottomConnectionPosition.RotateVector(Rotation, Vector3Int.zero);
            return ((Vector3)bottomNeighbourPosition + Position) / 2f;
        }

        private Vector3 GetTopConnectionPoint()
        {
            var topNeighbourPosition = Position + _topConnectionPosition.RotateVector(Rotation, Vector3Int.zero);
            var sameHightPosition = new Vector3(Position.x, topNeighbourPosition.y, Position.z);
            return (sameHightPosition + topNeighbourPosition) / 2f;
        }
    }
}