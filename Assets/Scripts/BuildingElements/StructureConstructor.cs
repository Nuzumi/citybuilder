﻿using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.Services.SaveLoadServices;
using Assets.Scripts.Services.UnitMovementServices;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.BuildingElements
{
    public class StructureConstructor : StructureElement, IStructureStateConsumer, IStructureStorage
    {
        public IReadOnlyDictionary<ResourceType, int> MaxStorage => _maxStorage;

        [SerializeField] private StructureStorageData _constructionCost;
        [SerializeField] private BaseStructurePathElement _structurePathElement;

        private UnitMovementService _unitMovementService;
        private ResourceService _resourceService;

        private readonly Dictionary<ResourceType, int> _maxStorage = new();
        private readonly Dictionary<ResourceType, int> _storedResources = new();

        private StructureState _state;
        private UnitIntrestPoint _intrestPoint;

        [Inject]
        public void Initialize(UnitMovementService unitMovementService, ResourceService resourceService)
        {
            _unitMovementService = unitMovementService;
            _resourceService = resourceService;

            foreach (var resource in _constructionCost.capacityCreated)
            {
                _maxStorage[resource.type] = resource.amount;
                _storedResources[resource.type] = 0;
            }
        }

        public bool CanRemoveResource(ResourceData resource) => false;

        public bool CanStoreResource(ResourceData resource)
        {
            if (!_maxStorage.ContainsKey(resource.type))
            {
                return false;
            }

            return _storedResources[resource.type] + resource.amount <= _maxStorage[resource.type];
        }

        public List<ResourceData> GetAllResources() => _storedResources.Select(pair => new ResourceData() { type = pair.Key, amount = pair.Value}).ToList();

        public void RemoveResource(ResourceData resource) {}

        public void StoreResource(ResourceData resource)
        {
            _storedResources[resource.type] += resource.amount;
            _resourceService.GetResource(resource.type).Amount.Value -= resource.amount;

            TryCompleteStructure();
        }

        public void SetStructureState(StructureState structureState)
        {
            _state = structureState;

            TryRegisterAsIntrestPoint();
            _state.StateChanged += OnStateChange;
        }

        public override void LoadStructureSaveState(StructureSaveState state)
        {
            foreach (var storedResource in state.storedResources)
            {
                _storedResources[storedResource.type] = storedResource.amount;
            }
        }

        public override void UpdateStructureSaveState(StructureSaveState state)
        {
            if(_state.Type != StructureStateType.UnderConstruction)
            {
                return;
            }

            state.storedResources = _storedResources.Select(pair => new ResourceData()
            {
                type = pair.Key,
                amount = pair.Value
            }).ToList();
        }

        private bool CanCompleteStructure()
        {
            foreach (var resourceType in _maxStorage.Keys)
            {
                var maxResource = _maxStorage[resourceType];
                var currentAmount = _storedResources[resourceType];

                if(maxResource != currentAmount)
                {
                    return false;
                }
            }

            return true;
        }

        private void OnStateChange()
        {
            TryRegisterAsIntrestPoint();
            TryCompleteStructure();
        }

        private void TryRegisterAsIntrestPoint()
        {
            if(_state.Type != StructureStateType.UnderConstruction)
            {
                return;
            }

            _intrestPoint = new UnitIntrestPoint(UnitIntrestPointType.Consumer, _structurePathElement.GetPathCells()[0].Position, this);
            _unitMovementService.RegisterIntrestPoint(_intrestPoint);
        }

        private Dictionary<ResourceType, int> GetMaxStorage()
        {
            var result = new Dictionary<ResourceType, int>();

            foreach (var resource in _constructionCost.capacityCreated)
            {
                result[resource.type] = resource.amount;
            }

            return result;
        }

        private void TryCompleteStructure()
        {
            if (_state.Type != StructureStateType.UnderConstruction)
            {
                return;
            }

            if (!CanCompleteStructure())
            {
                return;
            }

            _unitMovementService.RemoveIntrestPoint(_intrestPoint);
            _state.Type = StructureStateType.Completed;
        }

        public override void UpdateDescription(StructureDescription description)
        {
            if (_state.Type != StructureStateType.UnderConstruction)
            {
                return;
            }

            description.StoredResources.AddRange(GetAllResources());
            description.BuildingCost.AddRange(_maxStorage.Select(p => new ResourceData() { type = p.Key, amount = p.Value }));
            description.StructureTooltipType = Ui.StructureTooltip.StructureTooltipType.UnderContruction;
        }
    }
}