﻿using Assets.Scripts.Services.PathServices;
using Assets.Scripts.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{

    public class GenericStructurePathElement : BaseStructurePathElement
    {
        [SerializeField] private Vector3Int _positionModifier;
        [SerializeField] private List<Vector3Int> _connectionPositions;
        [SerializeField] private List<Vector3Int> _emptyConnections;

        private Vector3Int Position => transform.position.ToIntVector() + _positionModifier.RotateVector(Rotation, Vector3Int.zero);
        private Quaternion Rotation => transform.localRotation;

        public override List<PathCell> GetPathCells()
        {
            var cell = new PathCell()
            {
                Position = Position,
                NeighbourPriority = 0,
                NeighboursConnectionPoints = new(),
                RemovedNeighboursConnectionsPoints = new()
            };

            foreach (var neighbourPosition in _connectionPositions)
            {
                var cellPosition = Position + neighbourPosition.RotateVector(Rotation, Vector3Int.zero);
                cell.NeighboursConnectionPoints[cellPosition] = GetConnectionPoint(cellPosition);
            }

            foreach (var neighbourPosition in _emptyConnections)
            {
                var cellPosition = Position + neighbourPosition.RotateVector(Rotation, Vector3Int.zero);
                cell.NeighboursConnectionPoints[cellPosition] = null;
            }

            return new List<PathCell>() { cell };
        }

        private Vector3 GetConnectionPoint(Vector3 neighbourCell) => (Position + neighbourCell) / 2f;
    }
}