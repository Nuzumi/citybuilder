﻿using Assets.Scripts.Services.PathServices;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{
    public class EmptyStructurePathElement : BaseStructurePathElement
    {
        public override List<PathCell> GetPathCells() => new ();
    }
}