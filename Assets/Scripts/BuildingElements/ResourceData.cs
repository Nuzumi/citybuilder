﻿using Assets.Scripts.Services.ResourcesServices;
using System;

namespace Assets.Scripts.BuildingElements
{
    [Serializable]
    public class ResourceData
    {
        public ResourceType type;
        public int amount;

        public ResourceData()
        {
        }

        public ResourceData(ResourceData resourceData)
        {
            type = resourceData.type;
            amount = resourceData.amount;
        }
    }
}