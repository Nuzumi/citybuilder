﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{
    [CreateAssetMenu(menuName = "Data/" + nameof(ResourceGeneratorData))]
    public class ResourceGeneratorData : ScriptableObject
    {
        public int timerTicksToComplete;
        public List<ResourceData> generatedResources;
        public List<ResourceData> generationCost;
    }
}