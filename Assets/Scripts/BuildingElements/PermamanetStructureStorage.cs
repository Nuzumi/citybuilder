﻿using Assets.Scripts.Services.ResourcesServices;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Scripts.Services.SaveLoadServices;
using Assets.Scripts.Services.UnitMovementServices;
using System.Linq;
using Assets.Scripts.Services.BuildingServices;

namespace Assets.Scripts.BuildingElements
{
    public class PermamanetStructureStorage : StructureElement, IStructureStorage, IStructureStateConsumer
    {
        public IReadOnlyDictionary<ResourceType, int> MaxStorage => GetMaxStorageDictionary();

        [SerializeField] private PermamentStorageData _storageData;
        [SerializeField] private BaseStructurePathElement _structurePathElement;

        private UnitMovementService _unitMovementService;

        private StructureState _state;
        private Dictionary<ResourceType, int> _storedResources = new();

        [Inject]
        public void Initialize(UnitMovementService unitMovementService)
        {
            _unitMovementService = unitMovementService;
        }

        public List<ResourceData> GetAllResources()
        {
            return _storedResources.Select(pair => new ResourceData() { type = pair.Key, amount = pair.Value }).ToList();
        }

        public bool CanStoreResource(ResourceData resource)
        {
            if (!_storageData.possibleResources.Contains(resource.type))
            {
                return false;
            }

            var currentCapacity = _storedResources.Values.Sum();
            return _storageData.capacity - currentCapacity - resource.amount > 0;
        }

        public void StoreResource(ResourceData resource)
        {
            if (!_storedResources.ContainsKey(resource.type))
            {
                _storedResources.Add(resource.type, 0);
            }

            _storedResources[resource.type] += resource.amount;
        }

        public bool CanRemoveResource(ResourceData resource)
        {
            if (!_storedResources.ContainsKey(resource.type))
            {
                return false;
            }

            return _storedResources[resource.type] - resource.amount >= 0;
        }

        public void RemoveResource(ResourceData resource)
        {
            _storedResources[resource.type] -= resource.amount;
        }

        public override void LoadStructureSaveState(StructureSaveState state)
        {
            if(state.structureStateType != StructureStateType.Completed)
            {
                return;
            }

            foreach (var storedResource in state.storedResources)
            {
                _storedResources[storedResource.type] = storedResource.amount;
            }
        }

        public override void UpdateStructureSaveState(StructureSaveState state)
        {
            if (state.structureStateType != StructureStateType.Completed)
            {
                return;
            }

            state.storedResources = _storedResources.Select(pair => new ResourceData()
            {
                type = pair.Key,
                amount = pair.Value
            }).ToList();
        }

        private Dictionary<ResourceType, int> GetMaxStorageDictionary()
        {
            var storageDictionary = new Dictionary<ResourceType, int>();

            foreach (var type in _storedResources.Keys)
            {
                storageDictionary[type] = _storageData.capacity;
            }

            return storageDictionary;
        }

        public void SetStructureState(StructureState structureState)
        {
            _state = structureState;
            TryRegisterStorage();
            _state.StateChanged += TryRegisterStorage;
        }

        private void TryRegisterStorage()
        {
            if (_state.Type != StructureStateType.Completed)
            {
                return;
            }

            _unitMovementService.RegisterIntrestPoint(new UnitIntrestPoint(UnitIntrestPointType.Storage, _structurePathElement.GetPathCells()[0].Position, this));
        }

        public override void UpdateDescription(StructureDescription description)
        {
            if(_state.Type != StructureStateType.Completed)
            {
                return;
            }

            description.StructureTooltipType = Ui.StructureTooltip.StructureTooltipType.GeneralStructureStorage;
            description.StoredResources.AddRange(GetAllResources());
            description.GeneralStorageCapacity = _storageData.capacity;
        }
    }
}