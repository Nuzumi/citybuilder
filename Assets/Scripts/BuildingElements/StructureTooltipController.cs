﻿using Assets.Scripts.Services.StructuresServices;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Assets.Scripts.BuildingElements
{
    public class StructureTooltipController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IStructureStateConsumer
    {
        private StructureStateType StateType => _buildingElement.State;

        [SerializeField] private StructureBuildingElement _buildingElement;
        [SerializeField] private Collider _collider;

        private StructureTooltipService _tooltipService;
        private StructureState _structureState;

        [Inject]
        public void Initialize(StructureTooltipService tooltipService)
        {
            _tooltipService = tooltipService;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if(StateType == StructureStateType.None || StateType == StructureStateType.Placing)
            {
                return;
            }

            _tooltipService.ShowTooltip(_buildingElement.GetDescriptions());
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (StateType == StructureStateType.None || StateType == StructureStateType.Placing)
            {
                return;
            }

            _tooltipService.HideTooltip();
        }

        public void SetStructureState(StructureState structureState)
        {
            _structureState = structureState;
            TryActivateCollider();
            _structureState.StateChanged += TryActivateCollider;
        }

        private void TryActivateCollider()
        {
            _collider.enabled = _structureState.Type == StructureStateType.Completed || _structureState.Type == StructureStateType.UnderConstruction;
        }
    }
}