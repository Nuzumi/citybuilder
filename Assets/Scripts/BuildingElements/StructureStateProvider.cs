﻿using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.SaveLoadServices;
using System;
using Unity.VisualScripting;

namespace Assets.Scripts.BuildingElements
{
    public interface IStructureStateConsumer
    {
        void SetStructureState(StructureState structureState);
    }

    public enum StructureStateType
    {
        None,
        Placing,
        UnderConstruction,
        Completed
    }

    public class StructureState
    {
        public event Action StateChanged;

        public StructureStateType Type
        {
            get => _type;
            set
            {
                _type = value;
                StateChanged?.Invoke();
            }
        }

        private StructureStateType _type;
    }

    public class StructureStateProvider : StructureElement
    {
        private StructureState _state;

        public override void LoadStructureSaveState(StructureSaveState state)
        {
            _state.Type = state.structureStateType;
        }

        public override void UpdateDescription(StructureDescription description)
        {
            description.StructureState = _state.Type;
        }

        public override void UpdateStructureSaveState(StructureSaveState state)
        {
            state.structureStateType = _state.Type;
        }

        private void Awake()
        {
            _state = new StructureState();

            foreach (var consumer in transform.GetComponents<IStructureStateConsumer>())
            {
                consumer.SetStructureState(_state);
            }
            
            foreach (var consumer in transform.GetComponentsInChildren<IStructureStateConsumer>())
            {
                consumer.SetStructureState(_state);
            }
        }
    }
} 