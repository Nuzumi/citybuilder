﻿using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.SaveLoadServices;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{
    public abstract class StructureElement : MonoBehaviour
    {
        public abstract void UpdateStructureSaveState(StructureSaveState state);

        public abstract void LoadStructureSaveState(StructureSaveState state);

        public abstract void UpdateDescription(StructureDescription description);
    }
}