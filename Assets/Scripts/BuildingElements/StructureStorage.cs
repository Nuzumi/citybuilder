﻿using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.Services.BuildingServices;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;
using Assets.Scripts.Services.SaveLoadServices;
using Assets.Scripts.Services.UnitMovementServices;
using System.ComponentModel.Design;

namespace Assets.Scripts.BuildingElements
{

    public class StructureStorage : StructureElement, IStructureStorage, IStructureStateConsumer
    {
        public IReadOnlyDictionary<ResourceType, int> MaxStorage => _maxStorage;

        [SerializeField] private StructureStorageData _storageData;
        [SerializeField] private BaseStructurePathElement _structurePathElement;

        private ResourceService _resourceService;
        private UnitMovementService _unitMovementService;
        private StructureState _state;

        private readonly Dictionary<ResourceType, int> _maxStorage = new ();
        private readonly Dictionary<ResourceType, int> _storedResources = new();

        [Inject]
        public void Initialize(ResourceService resourceService, UnitMovementService unitMovementService)
        {
            _resourceService = resourceService;
            _unitMovementService = unitMovementService;
            InitializeStorage();
        }

        public List<ResourceData> GetAllResources()
        {
            return _storedResources.Select(pair => new ResourceData() { type = pair.Key, amount = pair.Value}).ToList();
        }

        public bool CanStoreResource(ResourceData resource)
        {
            if (!_maxStorage.ContainsKey(resource.type))
            {
                return false;
            }

            return _storedResources[resource.type] + resource.amount <= _maxStorage[resource.type];
        }

        public void StoreResource(ResourceData resource)
        {
            _storedResources[resource.type] += resource.amount;
        }

        public bool CanRemoveResource(ResourceData resource)
        {
            if (!_storedResources.ContainsKey(resource.type))
            {
                return false;
            }

            return _storedResources[resource.type] - resource.amount >= 0;
        }

        public void RemoveResource(ResourceData resource)
        {
            _storedResources[resource.type] -= resource.amount;
        }

        public override void UpdateStructureSaveState(StructureSaveState state)
        {
            state.storedResources = _storedResources.Select(pair => new ResourceData()
            {
                type = pair.Key,
                amount = pair.Value
            }).ToList();
        }

        public override void LoadStructureSaveState(StructureSaveState state)
        {
            foreach (var storedResource in state.storedResources)
            {
                _storedResources[storedResource.type] = storedResource.amount;
            }
        }

        private void InitializeStorage()
        {
            foreach (var resource in _storageData.capacityCreated)
            {
                _maxStorage[resource.type] = resource.amount;
                _storedResources[resource.type] = 0;
            }
        }

        //todo check if this is needed
        private void AddResourceCapacity()
        {
            foreach (var resourceCapacity in _storageData.capacityCreated)
            {
                var resource = _resourceService.GetResource(resourceCapacity.type);
                resource.Capacity.Value += resourceCapacity.amount;
            }
        }

        public void RegisterAsIntrestPoint(UnitIntrestPointType type)
        {
            _unitMovementService.RegisterIntrestPoint(new UnitIntrestPoint(type, _structurePathElement.GetPathCells()[0].Position, this));
        }

        private void TryRegisterAsStorage()
        {
            if (!_storageData.isPermamentStorage)
            {
                return;
            }

            RegisterAsIntrestPoint(UnitIntrestPointType.Storage);
        }

        public void SetStructureState(StructureState structureState)
        {
            _state = structureState;
            TryBegineActions();
            _state.StateChanged += TryBegineActions;
        }

        public void TryBegineActions()
        {
            if(_state.Type != StructureStateType.Completed)
            {
                return;
            }

            AddResourceCapacity();
            TryRegisterAsStorage();
        }

        public override void UpdateDescription(StructureDescription description)
        {
            if (_state.Type != StructureStateType.Completed)
            {
                return;
            }

            if (!_storageData.isPermamentStorage)
            {
                return;
            }
            
            description.StoredResources.AddRange(GetAllResources());
            description.MaxStoredResources.AddRange(_storageData.capacityCreated.Select(r => new ResourceData(r)));
            description.StructureTooltipType = Ui.StructureTooltip.StructureTooltipType.StructureStorage;
        }
    }
}