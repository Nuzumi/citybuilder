﻿using Assets.Scripts.Services.PathServices;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{
    public class CombineStructurePathElement : BaseStructurePathElement
    {
        [SerializeField] private List<BaseStructurePathElement> _pathElements;

        public override List<PathCell> GetPathCells()
        {
            return _pathElements.SelectMany(element => element.GetPathCells()).ToList();
        }
    }
}