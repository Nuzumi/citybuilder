using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Services.PathServices;
using Assets.Scripts.Services.SaveLoadServices;
using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.StructureGrid;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.BuildingElements
{

    public class StructureBuildingElement : MonoBehaviour, IStructureStateConsumer
    {
        private const int RotationAmount = 90;

        public StructureStateType State => _state.Type;
        public bool CanBeAtatchedToTakenCell => _grid.CanBeAtatchedToTakenCell;
        public (IEnumerable<Vector3Int> cells, GridCellType type) RequiredCells => _grid.GetRequiredCellsPosition(transform.position.ToIntVector() + _centerCellPosition, Rotation);
        public IEnumerable<Vector3Int> CreatedCells => _grid.GetCreatedCellsPositions(transform.position.ToIntVector() + _centerCellPosition, Rotation);
        public (IEnumerable<Vector3Int> cells, GridCellType type) TakenCells => _grid.GetTakenCellsPositions(transform.position.ToIntVector() + _centerCellPosition, Rotation);

        private Quaternion Rotation => transform.localRotation;

        [SerializeField] private List<StructureElement> _elements;
        [SerializeField] private StructureBuildingElementGrid _grid;
        [SerializeField] private Vector3Int _centerCellPosition;
        [SerializeField] private BaseStructurePathElement _pathElement;

        private int _structureId;
        private StructureState _state;

        public void FinishBuild(int structureId)
        {
            _structureId = structureId;
            _state.Type = StructureStateType.UnderConstruction;
        }

        public StructureSaveState GetStructureSaveState()
        {
            var state = new StructureSaveState()
            {
                structureId = _structureId
            };

            foreach (var element in _elements)
            {
                element.UpdateStructureSaveState(state);
            }

            return state;
        }

        public void LoadStructureSaveState(StructureSaveState state)
        {
            _structureId = state.structureId;

            foreach (var element in _elements)
            {
                element.LoadStructureSaveState(state);
            }
        }

        public List<PathCell> GetStructurePathCell() => _pathElement.GetPathCells();

        public void StartPlacementOnPosition(Vector3Int position)
        {
            transform.position = position - _centerCellPosition;
            _state.Type = StructureStateType.Placing;
        }

        public StructureDescription GetDescriptions()
        {
            var description = new StructureDescription()
            {
                StructureId = _structureId
            };

            foreach (var element in _elements)
            {
                element.UpdateDescription(description);
            }

            return description;
        }

        public void Rotate()
        {
            transform.RotateAround(transform.position, Vector3.up, RotationAmount);
        }

        public void SetStructureState(StructureState structureState)
        {
            _state = structureState;
        }
    }
}