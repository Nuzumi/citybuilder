﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.StructureGrid
{
    public abstract class BaseStructureGridCell : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IStructureGridCell
    {
        public Vector3Int GridPosition => new Vector3Int((int)transform.position.x, (int)transform.position.y, (int)transform.position.z);

        [SerializeField] private MeshRenderer _renderer;
        [SerializeField] private Material _defaultMaterial;
        [SerializeField] private Material _selectedMaterial;

        private Action<IStructureGridCell> _selected;

        private void Start()
        {
            _renderer.material = _defaultMaterial;
        }

        public void SetSelectedAction(Action<IStructureGridCell> selected)
        {
            _selected = selected;
        }

        public void ResetToDefaultState()
        {
            _renderer.material = _defaultMaterial;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _renderer.material = _selectedMaterial;
            _selected?.Invoke(this);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            ResetToDefaultState();
        }

        public void SetActive(bool active) => gameObject.SetActive(active);
    }
}