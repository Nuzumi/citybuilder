﻿using UnityEngine;

namespace Assets.Scripts.StructureGrid
{
    public interface IStructureGridCell
    {
        Vector3Int GridPosition { get; }
    }
}