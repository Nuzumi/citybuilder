﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.StructureGrid
{
    public class WallStructureGridCell : BaseStructureGridCell
    {
        [SerializeField] private List<WallRotation> _wallRotations;

        public void SetPosition(Vector3Int position, Vector3Int wallPosition)
        {
            transform.SetPositionAndRotation(position, _wallRotations.Find(p => p.relativePosition == wallPosition - position).rotation);
        }

        [Serializable]
        private class WallRotation
        {
            public Vector3Int relativePosition;
            public Quaternion rotation;
        }
    }
}