using UnityEngine;

namespace Assets.Scripts.StructureGrid
{

    public class StructureGridCell : BaseStructureGridCell
    {
        public void SetPosition(Vector3Int position)
        {
            transform.position = position;
        }
    }
}