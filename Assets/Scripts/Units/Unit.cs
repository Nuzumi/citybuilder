using Assets.Scripts.Services.PathServices;
using Assets.Scripts.Services.UnitMovementServices;
using Assets.Scripts.Utils;
using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Units
{
    public class Unit : MonoBehaviour
    {
        public Vector3Int Position => transform.position.ToIntVector();

        [SerializeField] private float _speed;

        private PathService _pathService;
        private UnitMovementService _unitMovementService;

        private UnitMoveData _moveData;
        private List<Vector3> _path;
        private int _currentPathIndex;
        private Tween _tween;
        private Action<Unit> _currentMoveAction;

        [Inject]
        public void Initialize(PathService pathService, UnitMovementService unitMovementService)
        {
            _pathService = pathService;
            _unitMovementService = unitMovementService;
            _unitMovementService.AddFreeUnit(this);
        }

        public void StartMove(UnitMoveData moveData)
        {
            _moveData = moveData;
            StartMove();
        }

        public void StopMove()
        {
            _tween.Kill();
            _moveData = null;
            _unitMovementService.AddFreeUnit(this);
        }

        private void PathFinished()
        {
            _currentMoveAction(this);
            StartMove();
        }

        private void StartMove()
        {
            if(_moveData == null || _moveData.unitIntrestPoints.Count == 0)
            {
                StopMove();
                return;
            }

            _currentPathIndex = 0;
            var point = _moveData.unitIntrestPoints[0];
            _moveData.unitIntrestPoints.RemoveAt(0);
            _currentMoveAction = point.Item2;
            _path = _pathService.GetPath(Position, point.Item1.Position);
            FollowPath();
        }

        private void FollowPath()
        {
            _tween.Kill();
            if (_currentPathIndex >= _path.Count)
            {
                PathFinished();
                return;
            }

            _tween = transform.DOMove(_path[_currentPathIndex], GetMoveDuration(_path[_currentPathIndex]))
                .SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    _tween.Kill();
                    _currentPathIndex++;
                    FollowPath();
                });
        }

        private float GetMoveDuration(Vector3 target)
        {
            return Vector3.Distance(transform.position, target) / _speed;
        }
    }
}