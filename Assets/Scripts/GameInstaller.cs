using Assets.Scripts.Services.ResourcesServices;
using Assets.Scripts.Services.SaveLoadServices;
using Assets.Scripts.Services.BuildingServices;
using Assets.Scripts.Ui;
using Services.UI;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Scripts.Services.StructuresServices;
using Assets.Scripts.Services.StructureGridServices;
using Assets.Scripts.StructureGrid;
using Assets.Scripts.Services.UiDefinitions;
using Assets.Scripts.Services.UI.Views;
using Assets.Scripts.Services.PathServices;
using Assets.Scripts.Services.UnitMovementServices;
using Assets.Scripts.MapGeneration;
using Assets.Scripts.Services.CaveServices;

namespace Assets.Scripts
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField] private UiElementsLayerData _layerData;
        [SerializeField] private UiElementsPrefabData _uiElementsPrefabData;
        [SerializeField] private StructuresPrefabProvider _structuresPrefabProvider;
        [SerializeField] private Transform _structuresParent;
        [SerializeField] private ResourceDefinitionProvider _resourceDefinitionProvider;
        [SerializeField] private StructureGridCell _cellPrefab;
        [SerializeField] private WallStructureGridCell _wallCellPrefab;
        [SerializeField] private CaveGeneratorPerlin _caveGenerator;
        [SerializeField] private MeshFilter _caveMesh;
        [SerializeField] private CaveCellTypesGenerator _caveCellTypesGenerator;
        [SerializeField] private CaveCellTypeTextureData _caveCellTypeTextureData;

        public override void InstallBindings()
        {
            InstallServices();
            InstallContextObjects();
            InstallHelperClasses();
        }

        private void InstallContextObjects()
        {
            Container.Bind<UiElementsLayerData>().FromInstance(_layerData);
            Container.Bind<UiElementsPrefabData>().FromInstance(_uiElementsPrefabData);
            Container.Bind<StructuresPrefabProvider>().FromInstance(_structuresPrefabProvider);
            Container.Bind<Transform>().FromInstance(_structuresParent);
            Container.Bind<ResourceDefinitionProvider>().FromInstance(_resourceDefinitionProvider);
            Container.Bind<StructureGridCell>().FromInstance(_cellPrefab);
            Container.Bind<WallStructureGridCell>().FromInstance(_wallCellPrefab);
            Container.Bind<CaveGeneratorPerlin>().FromInstance(_caveGenerator);
            Container.Bind<MeshFilter>().FromInstance(_caveMesh);
            Container.Bind<CaveCellTypesGenerator>().FromInstance(_caveCellTypesGenerator);
            Container.Bind<CaveCellTypeTextureData>().FromInstance(_caveCellTypeTextureData);
        }

        private void InstallServices()
        {
            Container.Bind<ViewsService>().AsSingle();
            Container.Bind<PopupsFactory>().AsSingle();
            Container.Bind<StructuresFactory>().AsSingle();
            Container.Bind<BuildingInputService>().AsSingle();
            Container.Bind<StructuresService>().AsSingle();
            Container.Bind<LoadService>().AsSingle();
            Container.Bind<MainMenuService>().AsSingle();
            Container.Bind<StructureTooltipService>().AsSingle();

            Container.BindInterfacesAndSelfTo<ResourceService>().AsSingle();
            Container.BindInterfacesAndSelfTo<ResourceUpdateTimer>().AsSingle();
            Container.BindInterfacesAndSelfTo<StructuresBuildingService>().AsSingle();
            Container.BindInterfacesAndSelfTo<SaveService>().AsSingle();
            Container.BindInterfacesAndSelfTo<StructureGridService>().AsSingle();
            Container.BindInterfacesAndSelfTo<UiDefinitionService>().AsSingle();
            Container.BindInterfacesAndSelfTo<PathService>().AsSingle();
            Container.BindInterfacesAndSelfTo<UnitMovementService>().AsSingle();
            Container.BindInterfacesAndSelfTo<PathCacheService>().AsSingle();
            Container.BindInterfacesAndSelfTo<CaveService>().AsSingle();
        }

        private void InstallHelperClasses()
        {
            Container.Bind<CaveMeshCreator>().AsSingle();
        }
    }
}